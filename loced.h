/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef LOCED_H
#define LOCED_H

#include <memory>

#include <QtWidgets/QMainWindow>
#include <QSplitter>

#include "ui_loced.h"

class Location;
class	FedMap;
class MapScene;
class Tiles;

typedef std::shared_ptr<Location>	LocPtr;

class LocEd : public QMainWindow
{
	Q_OBJECT

public:
	static const int	NO_SELECTION = -1;

private:
	static const int	MAX_DESC_CHARS = 1000;
	static const int	ADJ_BTN = 999;
	static LocEd		*main_window;

	enum	{ MAP_TAB, VOCAB_TAB, LIST_TAB };	// Which map tab we are on
	enum	{ MAP_INFO, EXIT_INFO, EVENT_INFO };// Which info tab we are on

	Ui::LocEdClass ui;

	FedMap	*fed_map;
	MapScene	*scene;
	Tiles		*tiles;
	QImage	planet_image;
	QImage	space_image;
	QString	system_name;
	QString	version;
	QString	data_directory;
	int		last_arrow_selected;
	bool		adj_selected;

	std::string	MakeFullOrbitName();

	QString	ExtractDataDir(QString file_path);

	bool	CheckForSave();
	bool	eventFilter(QObject *target,QEvent *event);

	void	DisableAllLocProperties();
	void	DisableMvtArrows(int except);
	void	DisplaySystemName();
	void	EnableEditor();
	void	EnableMvtArrows();
	void	LoadSettings();
	void	ProcessArrowClick(int direction);
	void	ProcessDetatchedMove(int direction);
	void	SaveSettings();
	void	SetMvtValidator();
	void	SetTabsForMap();
	void	SetUpConnections();
	void	SetUpManualEditConnections();
	void	SetUpMenuConnections();
	void	SetUpMiscConnections();
	void	SetUpMovementConnections();
	void	SetUpPropertyConnections();

private slots:
	// Menu Slots
	void	AboutActionSlot();
	void	CopyActionSlot();
	void	CutActionSlot();
	void	DeleteActionSlot();
	void	ExitActionSlot();
	void	FindPropertySlot();
	void	FindTextSlot();
	void	FontActionSlot();
	void	LoadActionSlot();
	void	MapBgColorActionSlot();
	void	NewActionSlot();
	void	PasteActionSlot();
	void	ResetActionSlot();
	void	SaveActionSlot();
	void	SaveAsActionSlot();

	// Movent arrow slots
	void	DownBtnSlot();
	void	EastBtnSlot();
	void	InBtnSlot();
	void	NEBtnSlot();
	void	NorthBtnSlot();
	void	NotAdjBtnSlot();
	void	NWBtnSlot();
	void	OutBtnSlot();
	void	SEBtnSlot();
	void	SouthBtnSlot();
	void	SWBtnSlot();
	void	UpBtnSlot();
	void	WestBtnSlot();

	// Manually edited location items
	void	DescTextChangedSlot();
	void	DownCompleteSlot();
	void	EastCompleteSlot();
	void	InCompleteSlot();
	void	NameCompleteSlot();
	void	NoExitCompleteSlot();
	void	NorthCompleteSlot();
	void	NECompleteSlot();
	void	NWCompleteSlot();
	void	OutCompleteSlot();
	void	SECompleteSlot();
	void	SouthCompleteSlot();
	void	SWCompleteSlot();
	void	UpCompleteSlot();
	void	WestCompleteSlot();

	void	EnterEventSlot();
	void	InRoomEventSlot();
	void	NoMoveEventSlot();
	void	SearchEventSlot();

	// property slots
	void	BarCheckSlot(bool state);
	void	CourierCheckSlot(bool state);
	void	CustomCheckSlot(bool state);
	void	ExchangeCheckSlot(bool state);
	void	FightingCheckSlot(bool state);
	void	HospitalCheckSlot(bool state);
	void	LinkCheckSlot(bool state);
	void	PeaceCheckSlot(bool state);
	void	PickupCheckSlot(bool state);
	void	ShipRepairsCheckSlot(bool state);
	void	ShipyardCheckSlot(bool state);
	void	SpaceCheckSlot(bool state);
	void	TeleportCheckSlot(bool state);
	void	WeaponsCheckSlot(bool state);

	// other slots
	void	LaunchEditCompleteSlot();
	void	OrbitEditCompleteSlot();
	void	SystemNameChangedSlot(const QString& new_text);
	void	TabChangeSlot(int new_tab);
	void	TitleEditCompleteSlot();

protected:
	void	closeEvent(QCloseEvent *event);

public:
	static LocEd	*GetMainWindow()				{ return main_window;			}

	LocEd(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~LocEd();

	std::string	GetDesc()							{ return(ui.desc_edit->toPlainText().toStdString());	}
	std::string GetSaveFileName();

	int	GetLastArrow()								{ return last_arrow_selected;	}

	bool	IsSpaceMap();

	void	ArrowsOn();
	void	ArrowsOff();
	void	AddToMiscText(const std::string& text);
	void	CanEdit(bool allow);
	void	ClearDesc()									{ ui.desc_edit->clear();		}
	void	ClearEvents();
	void	ClearExitListing();
	void	ClearLocation();
	void	ClearLocFlags();
	void	ClearLocName()								{ ui.name_edit->clear();		}
	void	ClearMiscEdit()							{ ui.misc_text_edit->clear();	}
	void	ClearVocab();
	void	DisplayDesc(const std::string& para);
	void	DisplayEvent(int which,const std::string& event);
	void	DisplayExit(int which,int where);
	void	DisplayFileName(std::string text);
	void	DisplayListing(std::string text);
	void	DisplayListingBold(std::string text,bool has_internal_format = false);
	void	DisplayListing2StartPos();
	void	DisplayLaunchLoc(const std::string& loc_no);
	void	DisplayLocFlag(int which);
	void	DisplayLocNo(int loc_no);
	void	DisplayLocsUsed(int num_locs);
	void	DisplayMapTitle(const std::string title);
	void	DisplayMapType();
	void	DisplayName(const std::string name);
	void	DisplayNoExit(const std::string& no_exit);
	void	DisplayOrbitLoc(const std::string& loc_no);
	void	DisplayVocab(const std::string& cmd,const std::string& event);
	void	GetVocab(std::map<std::string,std::string>& vocab);
	void	ResetCursor()							{ QApplication::setOverrideCursor(Qt::ArrowCursor);	}
	void	SetDetatchedButton(int how)		{ ui.not_adj_btn->setEnabled(how);	}
	void	SetLocProperties();
	void	SetMapProperties();
	void	SetMenuLoc(bool set_paste);
	void	SetTabForLoc();
	void	ShowErrorMessage(const std::string& mssg);
	void	UpdateLocLabel(QString text)		{ ui.loc_label->setText(text);		}
	void	UpdateLocNumLabel(QString text)	{ ui.loc_no_label->setText(text);	}
};

#endif // LOCED_H
