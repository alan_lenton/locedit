/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "tiles.h"


const QString Tiles::pix_files[] = 
{
	":/images/unused.png",			":/images/used.png",				":/images/selected.png",	
	":/images/bad.png",				":/images/used-hidden.png",	
	":/images/north-arrow.png",	":/images/ne-arrow.png",		":/images/east-arrow.png",
	":/images/se-arrow.png",		":/images/south-arrow.png",	":/images/sw-arrow.png",
	":/images/west-arrow.png",		":/images/nw-arrow.png",					
};

Tiles::Tiles()
{
	for(int count = 0; count < NUM_TILES;++count)
		pix_maps[count] = new QPixmap(pix_files[count]);
}

Tiles::~Tiles()	
{	
	for(int count = 0; count < NUM_TILES;++count)
		delete pix_maps[count];
}
