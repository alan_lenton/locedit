/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

// General Note: Don't use shared pointers for Qt stuff, unless you want 
// to get your knickers in a twist 

#include "findproperty.h"

#include "location.h"

FindProperty::FindProperty(QWidget *parent) : QDialog(parent) 
{
	ui.setupUi(this);
}


int	FindProperty::FlagToCheck()
{
	if(ui.space_btn->isChecked())				return Location::SPACE;
	if(ui.not_space_btn->isChecked())		return NOT_SPACE;
	if(ui.link_btn->isChecked())				return Location::LINK;
	if(ui.peace_btn->isChecked())				return Location::PEACE;
	if(ui.ship_yard_btn->isChecked())		return Location::SHIPYARD;
	if(ui.ship_repairs_btn->isChecked())	return Location::REPAIR;
	if(ui.hospital_btn->isChecked())			return Location::HOSPITAL;
	if(ui.exchange_btn->isChecked())			return Location::EXCHANGE;
	if(ui.cafe_bar_btn->isChecked())			return Location::BAR;
	if(ui.no_teleport_btn->isChecked())		return Location::TELEPORT;
	if(ui.custom_btn->isChecked())			return Location::CUSTOM;
	if(ui.courier_btn->isChecked())			return Location::COURIER;
	if(ui.pickup_btn->isChecked())			return Location::PICKUP;

	return NOT_FOUND;
}