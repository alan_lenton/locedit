/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "fedmap.h"

#include <cctype>

#include "location.h"
#include "loced.h"
#include "mapparser.h"
#include "scene.h"
#include "tiles.h"


LocPtr	FedMap::clip_board = nullptr;

FedMap::FedMap()
{
	version = 0;
	is_a_space_map = false;
	mvt_state = NORMAL;
	is_modified = false;
	current_selection = LocEd::NO_SELECTION;
	scene = nullptr;
	unmodified_edit_loc = nullptr;
}


void	FedMap::AddDesc(std::string& text,int loc_no)
{
	if(loc_no == -1)
		loc_no = current_selection;
	LocPtr	edit = Find(loc_no);
	if(edit != nullptr)
	{
		edit->AddDesc(text);
		is_modified = true;
	}
}

void	FedMap::AddLocation(LocPtr loc)
{
	loc_index[loc->Number()] = loc;
	is_modified = true;
}

bool	FedMap::AddMvtArrows(int from,int direction)
{
	int tile = FindMvtTile(from,direction);
	switch(direction)
	{
		case Location::NORTH:	scene->AddTile((from % HEIGHT)*2,((from/WIDTH)*2) - 1,tile);			break;
		case Location::NE:		scene->AddTile(((from % HEIGHT)*2) + 1,((from/WIDTH)*2) - 1,tile);	break;
		case Location::EAST:		scene->AddTile(((from % HEIGHT)*2) + 1,(from/WIDTH)*2,tile);			break;
		case Location::SE:		scene->AddTile(((from % HEIGHT)*2) + 1,((from/WIDTH)*2) + 1,tile);	break;
		case Location::SOUTH:	scene->AddTile((from % HEIGHT)*2,((from/WIDTH)*2) + 1,tile);			break;
		case Location::SW:		scene->AddTile(((from % HEIGHT)*2) - 1,((from/WIDTH)*2) + 1,tile);	break;
		case Location::WEST:		scene->AddTile(((from % HEIGHT)*2) - 1,(from/WIDTH)*2,tile);			break;
		case Location::NW:		scene->AddTile(((from % HEIGHT)*2) - 1,((from/WIDTH)*2) - 1,tile);	break;
	}

	is_modified = true;
	return true;
}

void	FedMap::AddName(std::string name,int loc_no)
{
	if(loc_no == CURRENT_LOC)
		loc_no = current_selection;
	LocPtr	current_loc = Find(current_selection);
	if(current_loc != nullptr)
		current_loc->AddName(name);
	is_modified = true;
}

void	FedMap::AddVocab(int loc_no)
{
	LocEd	*main_window = LocEd::GetMainWindow();
	std::map<std::string,std::string> new_vocab;
	main_window->GetVocab(new_vocab);

	ClearVocab(loc_no);
	if(new_vocab.size() > 0)
	{
		for(auto iter = new_vocab.begin();iter != new_vocab.end();++iter)
			AddVocab(iter->first,iter->second,loc_no);
	}
}

void	FedMap::AddVocab(std::string cmd,std::string event,int loc_no)
{
	LocPtr	edit = Find(loc_no);
	if(edit != nullptr)
	{
		edit->AddVocab(cmd,event);
		is_modified = true;
	}
}

int	FedMap::Adjacent(int dir,int loc_no)
{
	int adjacent;
	switch(dir)
	{
		case Location::NORTH:	adjacent = loc_no - WIDTH;			break;
		case Location::NE:		adjacent = loc_no - WIDTH + 1;	break;
		case Location::EAST:		adjacent = loc_no + 1;				break;
		case Location::SE:		adjacent = loc_no + WIDTH + 1;	break;
		case Location::SOUTH:	adjacent = loc_no + WIDTH;			break;
		case Location::SW:		adjacent = loc_no + WIDTH - 1;	break;
		case Location::WEST:		adjacent = loc_no - 1;				break;
		case Location::NW:		adjacent = loc_no - WIDTH - 1;	break;
	}

	if(IsOffMap(adjacent))	// Check for off top or bottom
		return OFF_MAP;

	switch(dir)	// Check for wrap around on sides of map
	{
		case Location::NE:
		case Location::EAST:
		case Location::SE:	return((adjacent % WIDTH < loc_no % WIDTH) ? WRAP : adjacent);

		case Location::SW:
		case Location::WEST:
		case Location::NW:	return((adjacent % WIDTH > loc_no % WIDTH) ? WRAP : adjacent);

		default:	break;
	}

	return adjacent;
}

bool	FedMap::CheckMap()
{
	if(!is_a_space_map)
	{
		LocEd	*main_window = LocEd::GetMainWindow();
		if((launch_loc == "") || (orbit_loc == ""))
		{
			main_window->ShowErrorMessage("Your planet must have both a launch and\nan orbit location. Click the map tab\non the right hand side to get to them.");
			return false;
		}
		if(Find(std::atoi(launch_loc.c_str())) == nullptr)
		{
			main_window->ShowErrorMessage("The map's launch location must exist");
			return false;
		}
	}
	return true;
}

void	FedMap::ClearAllPlanetFlags()
{
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
	{
		iter->second->SetFlag(Location::EXCHANGE,false);
		iter->second->SetFlag(Location::SHIPYARD,false);
		iter->second->SetFlag(Location::REPAIR,false);
		iter->second->SetFlag(Location::HOSPITAL,false);
		iter->second->SetFlag(Location::INSURE,false);
		iter->second->SetFlag(Location::BAR,false);
		iter->second->SetFlag(Location::COURIER,false);
		iter->second->SetFlag(Location::UNLIT,false);
		iter->second->SetFlag(Location::TELEPORT,false);
		iter->second->SetFlag(Location::PICKUP,false);
		iter->second->SetFlag(Location::WEAPONS,false);
	}
	is_a_space_map = true;
	is_modified = true;
}

void	FedMap::ClearAllSpaceFlags()
{
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
	{
		iter->second->SetFlag(Location::SPACE,false);
		iter->second->SetFlag(Location::LINK,false);
		iter->second->SetFlag(Location::PEACE,false);
		iter->second->SetFlag(Location::FIGHTING,false);
	}
	is_a_space_map = false;
	is_modified = true;
}

void	FedMap::ClearDesc(int loc_no)
{
	LocPtr	edit = Find(loc_no);
	if(edit != nullptr)
	{
		edit->ClearDesc();
		is_modified = true;
	}
}

void	FedMap::ClearExit(int dir,int loc_no)
{
	int tile = FindMvtTile(loc_no,dir);
	switch(dir)
	{
		case Location::NORTH:	scene->RemoveTile((loc_no % HEIGHT)*2,((loc_no/WIDTH)*2) - 1,tile);			break;
		case Location::NE:		scene->RemoveTile(((loc_no % HEIGHT)*2) + 1,((loc_no/WIDTH)*2) - 1,tile);	break;
		case Location::EAST:		scene->RemoveTile(((loc_no % HEIGHT)*2) + 1,(loc_no/WIDTH)*2,tile);			break;
		case Location::SE:		scene->RemoveTile(((loc_no % HEIGHT)*2) + 1,((loc_no/WIDTH)*2) + 1,tile);	break;
		case Location::SOUTH:	scene->RemoveTile((loc_no % HEIGHT)*2,((loc_no/WIDTH)*2) + 1,tile);			break;
		case Location::SW:		scene->RemoveTile(((loc_no % HEIGHT)*2) - 1,((loc_no/WIDTH)*2) + 1,tile);	break;
		case Location::WEST:		scene->RemoveTile(((loc_no % HEIGHT)*2) - 1,(loc_no/WIDTH)*2,tile);			break;
		case Location::NW:		scene->RemoveTile(((loc_no % HEIGHT)*2) - 1,((loc_no/WIDTH)*2) - 1,tile);	break;
	}	
	is_modified = true;
}

void	FedMap::ClearLocationDisplay()
{
	LocEd	*main_window = LocEd::GetMainWindow();
	main_window->ClearDesc();
	main_window->ClearEvents();
	main_window->ClearExitListing();
	main_window->ClearLocFlags();
	main_window->ClearLocName();
	main_window->ClearVocab();
}

void	FedMap::ClearLocs()
{
	loc_index.clear();
	is_modified = true;
}

void	FedMap::ClearVocab(int loc_no)
{
	LocPtr	edit = Find(loc_no);
	if(edit != nullptr)
	{
		edit->ClearVocab();
		is_modified = true;
	}
}

void	FedMap::ClickAt(int abs_row,int abs_column)
{
	if((abs_row % 2 == 1) || (abs_column % 2 == 1))	// Not a location position
		return;
	
	LocEd	*main_win = LocEd::GetMainWindow();
	int loc_no = (abs_row/2) * WIDTH + abs_column/2;
	
	if(mvt_state == NORMAL)
	{
		LocPtr	loc = Find(loc_no);
		if(current_selection >= 0)
		{
			LocPtr	current_sel_loc = Find(current_selection);
			if((current_selection != loc_no) && (current_sel_loc != nullptr))
			{
				AddDesc(main_win->GetDesc(),current_selection);
				AddVocab(current_selection);
				DeselectLocTile(current_selection,current_sel_loc);
			}
		}

		if(loc != nullptr)
		{
			scene->ChangeTile((loc_no % HEIGHT)*2,(loc_no/WIDTH)*2,Tiles::SELECTED);
			DisplayInfo(loc);
		}
		else
		{
			NewLocation(loc_no);
			ClearLocationDisplay();
			SetFlag(Location::SPACE,loc_no,is_a_space_map);
			main_win->SetLocProperties();
		}

		current_selection = loc_no;
		MakeEditLoc(loc_no);
		main_win->ArrowsOn();
	}
	else
	{
		int dir = main_win->GetLastArrow();
		LocPtr current = Find(current_selection);
		if(current != nullptr)
		{
			current->SetExit(dir,loc_no);
			DisplayInfo(current);
			mvt_state = NORMAL;
			main_win->ResetCursor();
			main_win->ArrowsOn();
		}
	}
	main_win->SetTabForLoc();
	main_win->CanEdit(true);
	main_win->SetMenuLoc(clip_board != nullptr);
}

bool	FedMap::CopyLoc()
{
	auto	iter = loc_index.find(current_selection);
	if(iter !=  loc_index.end())
	{
		clip_board.reset(new Location(*(iter->second)));
		LocEd::GetMainWindow()->UpdateLocLabel("Location copied...");
		return true;
	}
	else
		return false;
}

bool	FedMap::CutLoc()
{
	auto	iter = loc_index.find(current_selection);
	if(iter !=  loc_index.end())
	{
		clip_board = iter->second;
		DeleteLoc();
		is_modified = true;
		return true;
	}
	else
		return false;
}

void	FedMap::DeleteLoc()
{
	auto	iter = loc_index.find(current_selection);
	if(iter !=  loc_index.end())
		loc_index.erase(iter);
 	current_selection = LocEd::NO_SELECTION;
	UpdateScene();
	is_modified = true;
}

void	FedMap::DeselectLocTile(int loc_no,LocPtr loc)
{
	int	tile = Tiles::USED;
	PtrExits	exits(loc->GetExits());
	if(exits->size() != 0)
	{
		for(auto iter2 = exits->begin();iter2 != exits->end();++iter2)
		{
			switch((*iter2).first)
			{
				case Location::UP:	// Drop through
				case Location::DOWN:	tile = Tiles::HIDDEN;	break;
				default:					tile = Tiles::USED;		break;
			}
		}
	}
	scene->ChangeTile((loc_no % HEIGHT)*2,(loc_no/WIDTH)*2,tile);
}

void	FedMap::DisplayInfo(LocPtr loc)
{
	LocEd	*main_window = LocEd::GetMainWindow();
	loc->DisplayName(main_window);
	loc->DisplayDesc(main_window);
	loc->DisplayLocNo(main_window);
	loc->DisplayExits(main_window);
	loc->DisplayEvents(main_window);
	loc->DisplayFlags(main_window);
	loc->DisplayVocab(main_window);
}

void	FedMap::DisplayLocInfo(int loc_no)
{
	LocPtr	loc = Find(loc_no);
	if(loc != nullptr)
		DisplayInfo(loc);
}

void	FedMap::DisplayMapInfo()
{
	LocEd	*main_window = LocEd::GetMainWindow();
	main_window->DisplayMapTitle(title);
	main_window->DisplayLaunchLoc(launch_loc);
	main_window->DisplayOrbitLoc(orbit_loc);
	if(is_a_space_map)
		SetMapType(SPACE_MAP);
	else
		SetMapType(PLANET_MAP);
	main_window->DisplayLocsUsed(loc_index.size());
}

LocPtr	FedMap::Find(int loc_no)
{
	auto	iter = loc_index.find(loc_no);
	if(iter !=  loc_index.end())
		return iter->second;
	else
		return nullptr;
}

// int	FedMap::FindMvtTile(int from, int direction,LocPtr to_loc)
int	FedMap::FindMvtTile(int from, int direction)
{
	switch(direction)
	{
		case Location::NORTH:	return Tiles::NORTH;
		case Location::NE:		return Tiles::NE;
		case Location::EAST:		return Tiles::EAST;
		case Location::SE:		return Tiles::SE;
		case Location::SOUTH:	return Tiles::SOUTH;
		case Location::SW:		return Tiles::SW;
		case Location::WEST:		return Tiles::WEST;
		case Location::NW:		return Tiles::NW;
		default:						return Tiles::NO_TILE;
	}
}

void	FedMap::FindNoSpaceFlag()
{
	LocEd	*main_window = LocEd::GetMainWindow();
	main_window->ClearMiscEdit();
	main_window->AddToMiscText("Find: No Space");

	if(loc_index.size() == 0)
	{
		main_window->AddToMiscText("No instances found");
		return;
	}

	bool found = false;
	std::ostringstream	buffer;
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
	{
		if(!iter->second->FlagIsSet(Location::SPACE))
		{
			buffer.str("");
			buffer << iter->first;
			main_window->AddToMiscText(buffer.str());
			found = true;
		}
	}
	if(!found)
		main_window->AddToMiscText("No instances found");
}

void	FedMap::FindProperty(int flag)
{
	static std::string	text[] = 
	{
		"Space", "Link", "Exchange", "Shipyard", "Repair", "Hospital", "Insure", "Peace",
		"Bar", "Courier", "Unlit", "Custom", "No Teleport", "Pickup", "Fighting", "Weapons"
	};

	LocEd	*main_window = LocEd::GetMainWindow();
	main_window->ClearMiscEdit();
	std::ostringstream	buffer;
	buffer << "Find: " << "'" << text[flag] << "'";
	main_window->AddToMiscText(buffer.str());

	if(loc_index.size() == 0)
	{
		main_window->AddToMiscText("No instances found");
		return;
	}

	bool found = false;
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
	{
		if(iter->second->FlagIsSet(flag))
		{
			buffer.str("");
			buffer << iter->first;
			main_window->AddToMiscText(buffer.str());
			found = true;
		}
	}
	if(!found)
		main_window->AddToMiscText("No instances found");
}

void	FedMap::FindText(const std::string& text)
{
	LocEd	*main_window = LocEd::GetMainWindow();
	main_window->ClearMiscEdit();
	std::ostringstream	buffer;
	buffer << "Find: " << "'" << text << "'";
	main_window->AddToMiscText(buffer.str());

	if(loc_index.size() == 0)
	{
		main_window->AddToMiscText("No instances found");
		return;
	}

	bool found = false;
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
	{
		if(iter->second->HasSubString(text))
		{
			buffer.str("");
			buffer << iter->first;
			main_window->AddToMiscText(buffer.str());
			found = true;
		}
	}
	if(!found)
		main_window->AddToMiscText("No instances found");
}

int	FedMap::GetExit(int loc_no, int which)
{
	LocPtr	loc = Find(loc_no);
	if(loc == nullptr)
		return -1;
	return(loc->GetExit(which));
}

std::unique_ptr<std::list<std::pair<int,int>>>	FedMap::GetExits(int loc_no)
{
	LocPtr	loc = Find(loc_no);
	if(loc == nullptr)
		return nullptr;
	return loc->GetExits();
}

void	FedMap::InitializeNewMap()
{
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
	{
		LocPtr temp = iter->second;
		loc_index.erase(iter);
	}

	file_name = "";
	title = "";
	launch_loc = "";
	orbit_loc = "";
	version = 0;

	scene->InitialiseMap();
	UpdateScene();
	DisplayMapInfo();
	is_modified = false;
	unmodified_edit_loc = nullptr;
}

bool	FedMap::IsAPossibleLink(int col, int row)
{
	if(((row % 2) == 0) || ((col % 2) == 0))
		return false;
	return true;
}

bool	FedMap::IsAPossibleLoc(int col,int row)
{
	if(((row % 2) == 1) || ((col % 2) == 1))
		return false;
	return true;
}

bool FedMap::IsOffMap(int loc_no)
{
	if((loc_no < 0) || (loc_no >= (HEIGHT * WIDTH)))
		return true;
	else
		return false;
}
	
// Bounding right hand side col is HEIGHT locs + (HEIGHT - 1) links, because there is
// no link col after the last loc and -1 again for starting at the numbering at zero.
bool	FedMap::IsOffMap(int col,int row)
{
	if((row < 0) || (row >= HEIGHT * 2 - 2))
		return OFF_MAP;
	if((col < 0) || (col >= WIDTH * 2 - 2))
		return OFF_MAP;
	return true;
}

void	FedMap::Load(const std::string& new_file_name)
{
	LocEd	*main_window = LocEd::GetMainWindow();	
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
	{
		LocPtr temp = iter->second;
		loc_index.erase(iter);
	}
	current_selection = LocEd::NO_SELECTION;

	MapParser map_parser(new_file_name,this);
	if(!map_parser.Parse())
		main_window->ShowErrorMessage("There was a problem with the\nfile you were trying to load.");
	file_name = new_file_name;
	scene->InitialiseMap();
	UpdateScene();
	if((loc_index.size() > 0) && loc_index.begin()->second->FlagIsSet(Location::SPACE))
		is_a_space_map = true;
	else
		is_a_space_map = false;
	DisplayMapInfo();
	main_window->CanEdit(false);
	main_window->DisplayFileName(file_name);
	is_modified = false;
}

int	FedMap::LocNumber(int col,int row)
{
	if(!IsAPossibleLoc(row,col))
		return NOT_A_LOC;
	if(IsOffMap(row,col))
		return OFF_MAP;
	return((row * HEIGHT/2) + col/2);
}

// Makes a -COPY- of a location about to be edited  
// in case the user wishes to cancel the editing
void	FedMap::MakeEditLoc(int loc_no)
{
	LocPtr loc_ptr = Find(loc_no);
	if(loc_ptr == nullptr)
		return;

	unmodified_edit_loc.reset(new Location(*loc_ptr));
}

void	FedMap::MouseAt(int abs_row,int abs_column)
{
	LocEd	*main_win = LocEd::GetMainWindow();

	if((abs_row % 2 == 1) || (abs_column % 2 == 1))	// Not a location position
	{
		main_win->UpdateLocNumLabel("");
		return;
	}
	
	int loc_no = (abs_row/2) * WIDTH + abs_column/2;
	LocPtr loc = Find(loc_no);

	main_win->UpdateLocNumLabel(QString::number(loc_no));
	if(loc != nullptr)
		main_win->UpdateLocLabel(QString::fromStdString(loc->GetName()));
	else
		main_win->UpdateLocLabel("");
}

LocPtr	FedMap::NewLocation(int loc_no)
{
	LocPtr loc(new Location(loc_no));
	AddLocation(loc);
	LocEd *main_window = LocEd::GetMainWindow();
	main_window->DisplayLocNo(loc_no);
	if(current_selection == LocEd::NO_SELECTION)
		scene->AddTile((loc_no % HEIGHT)*2,(loc_no/WIDTH)*2,Tiles::SELECTED);
	else
		scene->ChangeTile((loc_no % HEIGHT)*2,(loc_no/WIDTH)*2,Tiles::SELECTED);
	current_selection = loc_no;
	is_modified = true;
	return loc;
}

int	FedMap::OppositeCardinalDirection(int direction)
{
	if(direction < 4)
		return(direction + 4);
	if(direction < 8)
		return(direction - 4);
	return -1;
}

void	FedMap::PasteLoc()
{
	if(clip_board == nullptr)
		return;

	// Clear the exits and make sure flags are appropriate for either space of planet
	for(int count = Location::NORTH;count < Location::MAX_EXITS;++count)
		clip_board->ClearExit(count);
	if(LocEd::GetMainWindow()->IsSpaceMap())
	{
		clip_board->SetFlag(Location::SPACE,true);
		for(int count = Location::EXCHANGE;count < Location::MAX_FLAGS;++count)
		{
			if(count == Location::PEACE)
				continue;
			clip_board->SetFlag(count,false);
		}
	}
	else
	{
		clip_board->SetFlag(Location::SPACE,false);
		clip_board->SetFlag(Location::LINK,false);
		clip_board->SetFlag(Location::PEACE,false);
	}
	clip_board->SetLocNo(current_selection);
	
	// and switch in the new location
	auto	iter = loc_index.find(current_selection);
	if(iter !=  loc_index.end())
		iter->second.reset(new Location(*clip_board));
	else
		AddLocation(std::make_shared<Location>(*(new Location(*clip_board))));
	DisplayLocInfo(current_selection);
	is_modified = true;
}

void	FedMap::ReloadLoc()
{
	auto	iter = loc_index.find(current_selection);
	if(iter !=  loc_index.end())
	{
		iter->second.reset(new Location(*unmodified_edit_loc));
		DisplayLocInfo(current_selection);
		UpdateScene();
		is_modified = true;
	}
}

std::string&	FedMap::SanitizeStdString(std::string& text)
{
	int len = text.size();
	for(int count = 0;count < len;++count)
	{
		if((std::isalnum(text[count]) == 0) && (text[count] != ' '))
			text[count] = 'x';
	}
	return text;
}

std::string&	FedMap::SanitizeTitle(std::string& title_string)
{
	SanitizeStdString(title_string);
	title_string[0] = std::toupper(title_string[0]);
	int len = title_string.size();
	for(int count = 1;count < len;++count)
	{
		if(title_string[count - 1] == ' ')
			title_string[count] = std::toupper(title_string[count]);
	}
	return title_string;
}

void	FedMap::Save(bool do_map_check)
{
	LocEd	*main_win = LocEd::GetMainWindow();
	std::ofstream file;

	if(current_selection != LocEd::NO_SELECTION)
		SetDesc(main_win->GetDesc());

	if(do_map_check)
	{
		if(!CheckMap())
			return;
	}

	if(file_name == "")
	{
		if((file_name = main_win->GetSaveFileName()) == "")
			return;	// User cancelled
	}

	file.open(file_name);
	if(!file)
	{
		main_win->ShowErrorMessage("I'm sorry. I'm unable to\nopen a file with that name.");
		return;
	}

	file << "<?xml  version=\"1.0\"?>\n";
	file << "<fed2-map title='" << Location::EscapeXML(title) << "' version='" << ++version << "'";
	if(is_a_space_map)
		file << ">\n";
	else
		file << " editor='standard' from='" << launch_loc << "' to='" << orbit_loc << "'>\n";
	file << "\n";

	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
		iter->second->Save(file);
	file << "</fed2-map>\n";
	main_win->DisplayFileName(file_name);
	is_modified = false;
}

void	FedMap::SaveAs(const std::string new_name)
{
	file_name = new_name;
	Save(false);
}

void	FedMap::SetAllSpaceFlags()
{
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
		iter->second->SetFlag(Location::SPACE,true);
	is_a_space_map = true;
	is_modified = true;
}

void	FedMap::SetDesc(std::string text)
{
	LocPtr	selected = Find(current_selection);
	if(selected != nullptr)
	{
		selected->ClearDesc();
		selected->AddDesc(text);
	}
}

void	FedMap::SetEvent(int which,std::string event)
{
	LocPtr	selected = Find(current_selection);
	if(selected != nullptr)
	{
		selected->SetEvent(which,event);
		is_modified = true;
	}
}

bool	FedMap::SetExit(int dir,int to,int loc_no)
{
	LocPtr	from = Find(loc_no);
	LocPtr	dest = Find(to);
	if((from != nullptr) && (dest != nullptr))
	{
		from->SetExit(dir,to);
		is_modified = true;
		return true;
	}
	return false;
}

void	FedMap::SetExit(int which,int loc_no)
{
	LocPtr	selected = Find(current_selection);
	if(selected != nullptr)
	{
		selected->SetExit(which,loc_no);
		is_modified = true;
	}
}

bool	FedMap::SetFlag(int which,int loc_no,bool value)
{
	static const std::string	no_exch_weapon("A location cannot be both\nan exchange and a weapon shop!");
	static const std::string	no_link_fighting("No fighting allowed in a link location!");

	if(loc_no == CURRENT_LOC)
		loc_no = current_selection;

	LocPtr	loc = Find(loc_no);
	if(loc == nullptr)	
		return false;
	else
	{
		if((which == Location::WEAPONS) && value)
		{
			if(loc->FlagIsSet(Location::EXCHANGE))
			{
				LocEd::GetMainWindow()->ShowErrorMessage(no_exch_weapon);
				return false;
			}
		}
		if((which == Location::EXCHANGE) && value)
		{
			if(loc->FlagIsSet(Location::WEAPONS))
			{
				LocEd::GetMainWindow()->ShowErrorMessage(no_exch_weapon);
				return false;
			}
		}

		if((which == Location::FIGHTING) && value)
		{
			if(loc->FlagIsSet(Location::LINK))
			{
				LocEd::GetMainWindow()->ShowErrorMessage(no_link_fighting);
				return false;
			}
		}
		if((which == Location::LINK) && value)
		{
			if(loc->FlagIsSet(Location::FIGHTING))
			{
				LocEd::GetMainWindow()->ShowErrorMessage(no_link_fighting);
				return false;
			}
		}

		loc->SetFlag(which,value);
		is_modified = true;
		return true;
	}
	return false;
}

void	FedMap::SetLaunch(std::string text)		
{
	launch_loc = text;
	LocEd::GetMainWindow()->DisplayLaunchLoc(launch_loc);
	is_modified = true;
}

void	FedMap::SetMapType(int map_type)		
{ 
	is_a_space_map = (map_type == SPACE_MAP);
	LocEd::GetMainWindow()->DisplayMapType();
	if(is_a_space_map)
		ClearAllPlanetFlags();
	else
		ClearAllSpaceFlags();
} 

void	FedMap::SetNoExit(std::string loc_no_exit)
{
	LocPtr	selected = Find(current_selection);
	if(selected != nullptr)
	{
		selected->SetNoExit(loc_no_exit);
		is_modified = true;
	}
}

void	FedMap::SetOrbit(std::string text)		
{
	if(orbit_loc == text)
		return;
	orbit_loc = text;
	LocEd::GetMainWindow()->DisplayOrbitLoc(orbit_loc);
	is_modified = true;
}

void	FedMap::SetTitle(std::string text)		
{
	title = text;
	LocEd::GetMainWindow()->DisplayMapTitle(title);
	is_modified = true;
}

bool	FedMap::ToggleExit(int dir,int loc_no)
{
	// Toggling the exit off
	LocPtr	loc = Find(loc_no);
	if(loc == nullptr)	
		return false;
	if(loc->GetExit(dir) != -1)
	{
		loc->ClearExit(dir);
		ClearExit(dir,loc_no);
		LocEd::GetMainWindow()->SetDetatchedButton(false);
		is_modified = true;
		return true;
	}

	// Toggling the exit on
	int	adjacent = Adjacent(dir,loc_no);
	loc->SetExit(dir,adjacent);
	LocEd::GetMainWindow()->SetDetatchedButton(true);
	is_modified = true;
	return true;
}

void	FedMap::UpdateLocTile(int loc_no,LocPtr loc)
{
	PtrExits	exits(loc->GetExits());
	int	tile = Tiles::USED;
	bool	hidden_exits = false;
	if(exits->size() != 0)
	{
		for(auto iter2 = exits->begin();iter2 != exits->end();++iter2)
		{
			switch((*iter2).first)
			{
				case Location::NORTH:
				case Location::NE:
				case Location::EAST:
				case Location::SE:
				case Location::SOUTH:
				case Location::SW:
				case Location::WEST:		// Drop through
				case Location::NW:		AddMvtArrows(loc_no,(*iter2).first);			break;
				case Location::UP:	
				case Location::DOWN:		tile = Tiles::HIDDEN;	hidden_exits = true;	break;
				default:						tile = Tiles::USED;									break;
			}
		}
	}
	if(hidden_exits)
		tile = Tiles::HIDDEN;
	if(loc_no == current_selection)
		tile = Tiles::SELECTED;
	scene->ChangeTile((loc_no % HEIGHT)*2,(loc_no/WIDTH)*2,tile);
}

void	FedMap::UpdateScene()
{
	scene->InitialiseMap();
	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
		UpdateLocTile(iter->first,iter->second);
}

void	FedMap::UpdateScene(int loc_no)
{
	auto	iter = loc_index.find(loc_no);
	if(iter != loc_index.end())
		UpdateLocTile(iter->first,iter->second);
}

void	FedMap::WriteListing() const
{
	if(loc_index.size() == 0)
		return;

	LocEd	*main_window = LocEd::GetMainWindow();
	std::ostringstream	buffer;

	main_window->DisplayListing("                  ");
	buffer << title << " -  Version " << version << (is_modified ? "  modified" : "");
	main_window->DisplayListingBold(buffer.str());
	buffer.str("");
	buffer << "Launch: " << ((launch_loc != "") ? launch_loc : "none");
	buffer << "    Orbit: " << ((orbit_loc != "") ? orbit_loc : "none") << "\n";
	main_window->DisplayListing(buffer.str());

	for(auto iter = loc_index.begin();iter != loc_index.end();++iter)
		iter->second->WriteListing();
	main_window->DisplayListing2StartPos();
}


/* ---------------------- Work in Progress ---------------------- */

