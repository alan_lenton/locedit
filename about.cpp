
/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

// General Note: Don't use shared pointers for Qt stuff, unless you want 
// to get your knickers in a twist 

#include "about.h"

#include <QPainter>

About::About(QWidget *parent,QString& version) : QDialog(parent) 
{
	ui.setupUi(this);
	ui.version_label->setText(QString("Version ") + version);
	picture = QImage(":/images/about.png");
	ui.image_widget->installEventFilter(this);
}

bool About::eventFilter(QObject *target,QEvent *event)
{
	if((target == ui.image_widget) && (event->type() == QEvent::Paint))
	{
		QPainter	painter(ui.image_widget);
		painter.drawImage(0,0,picture);
		return(true);
	}
	return(QWidget::eventFilter(target,event));
}

