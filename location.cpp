/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "location.h"

#include <cstring>
#include <cctype>

#include "loced.h"
#include "mapparser.h"

const std::string	Location::triggers[] = { "enter","no-exit","in-room","search" };
const std::string	Location::directions[] = 
{ "n","ne","e","se","s","sw","w","nw","up","down","in","out" };
const std::string	Location::list_triggers[] = { "enter    ","no-exit  ","in-room  ","search    " };
const std::string	Location::list_directions[] = 
{ "N","NE","E","SE","S","SW","W","NW","Up","Down","In","Out" };


Location::Location()
{
	loc_no = INVALID_LOC;
	Clear();
}

Location::Location(int loc_num)
{
	loc_no = loc_num;
	Clear();
}

Location::Location(const Location& rhs)
{
	loc_no = rhs.loc_no;
	name = rhs.name;
	no_exit = rhs.no_exit;

	desc = rhs.desc;
	vocab = rhs.vocab;
	events = rhs.events;
	exits =  rhs.exits;
	flags = rhs.flags;
}


void	Location::AddDesc(std::string loc_desc,bool clear_first)
{
	if(clear_first)
		desc.clear();
	char	*buffer = new char[loc_desc.length() + 1];
	std::strcpy(buffer,loc_desc.c_str());
	char	*token = std::strtok(buffer,"\n\r");
	std::string	token_string;
	while(token != 0)
	{
		token_string = token;
		if(token_string.length() > 2)	// It's not just \n\r
			desc.push_back(Strip(token_string));
		token = std::strtok(0,"\n\r");
	}
	delete [] buffer;
}

void	Location::AddVocab(std::string cmd,std::string event)
{
	vocab[Strip(cmd)] = Strip(event);
}

void	Location::Clear()
{
	name.clear();
	desc.clear();
	no_exit.clear();
	for(auto iter = events.begin();iter != events.end();++iter)
		(*iter).clear();
	for(auto iter = exits.begin();iter != exits.end();++iter)
		*iter = NO_EXIT;
	flags.reset();
	vocab.clear();
}

void	Location::DisplayDesc(LocEd *main_window)
{
	main_window->ClearDesc();
	for(auto iter = desc.begin();iter != desc.end();++iter)
		main_window->DisplayDesc(*iter);
}

void	Location::DisplayEvents(LocEd *main_window)
{
	main_window->ClearEvents();
	for(int count = 0;count < MAX_EVENTS;++count)
	{
		if(events[count] != "")
			main_window->DisplayEvent(count,events[count]);
	}
}

void	Location::DisplayExits(LocEd *main_window)
{
	main_window->ClearExitListing();
	for(int count = NORTH;count < MAX_EXITS;++count)
	{
		if(exits[count] != NO_EXIT)
			main_window->DisplayExit(count,exits[count]);
	}
	if(no_exit != "")
		main_window->DisplayNoExit(no_exit);
}

void	Location::DisplayFlags(LocEd *main_window)
{
	main_window->ClearLocFlags();
	if(HasFlags())
	{
		for(int count = SPACE;count < MAX_FLAGS;++count)
		{
			if(flags.test(count))
				main_window->DisplayLocFlag(count);
		}
	}
}

void	Location::DisplayLocNo(LocEd *main_window)
{
	main_window->DisplayLocNo(loc_no);
}

void	Location::DisplayName(LocEd *main_window)
{
	main_window->DisplayName(name);
}

void	Location::DisplayVocab(LocEd *main_window)
{
	main_window->ClearVocab();
	if(vocab.size() == 0)
		return;

	for(auto iter = vocab.begin();iter != vocab.end();++iter)
		main_window->DisplayVocab(iter->first,iter->second);
}

const std::string&	Location::EscapeXML(const std::string& text)
{
	static std::string	ret;
	std::ostringstream	xml_buffer;

	int len = text.length();
	for(int count = 0;count < len;++count)
	{
		switch(text[count])
		{
			case  '<':	xml_buffer << "&lt;";	break;
			case  '>':	xml_buffer << "&gt;";	break;
			case  '&':	xml_buffer << "&amp;";	break;
			case '\'':	xml_buffer << "&apos;";	break;
			case '\"':	xml_buffer << "&quot;";	break;
			default:		if(isascii(text[count]) == 0)	// <cctype> pulling isascii into the global namespace!
								xml_buffer << '#';
							else
								xml_buffer << text[count];
							break;
		}
	}
	ret = xml_buffer.str();
	return ret;
}

std::string	Location::GetDesc() const
{
	static	std::string	ret_val;
	ret_val.clear();
	for(auto iter = desc.begin();iter != desc.end();++iter)
	{
		ret_val += *iter;
		ret_val += "\n\r";
	}
	return ret_val;
}

PtrExits	Location::GetExits()
{
	PtrExits	return_list(new std::list<std::pair<int,int>>);
	for(int count = NORTH;count < MAX_EXITS;++count)
	{
		if(exits[count] != NO_EXIT)
		{
			std::pair<int,int> p(count,exits[count]);
			return_list->push_back(p);
		}
	}
	return return_list;
}

bool	Location::HasSubString(const std::string& text)
{
	if(name.find(text) != std::string::npos)
		return true;
	for(auto iter = desc.begin();iter != desc.end();++iter)
	{
		if((*iter).find(text) != std::string::npos)
			return true;
	}
	return false;
}

void	Location::Save(std::ofstream& file)
{
	if(name == "")				{ name = "xxx"; }
	if(desc.size() == 0)		{ desc.push_back("xxx"); }

	file << "  <location num='" << loc_no << "' " << SaveFlags() << ">\n";
	SaveName(file);
	SaveDesc(file);
	SaveExits(file);
	SaveEvents(file);
	SaveVocab(file);
	file << "  </location>\n\n";
}

void	Location::SaveDesc(std::ofstream& file)
{
	for(auto iter = desc.begin();iter != desc.end();++iter)
		file << "    <desc>" << EscapeXML(*iter) << "</desc>\n";
}

void	Location::SaveEvents(std::ofstream& file)
{
	bool no_events(true);
	for(int count = 0;count < MAX_EVENTS;++count)
	{
		if(events[count] != "")
		{
			no_events = false;
			break;
		}
	}
	
	if(no_events)
		return;

	file << "    <events";
	for(int count = 0;count < MAX_EVENTS;++count)
	{
		if(events[count] != "")
			file << " " << triggers[count] << "='" << events[count] << "'";
	}
	file << "/>\n";
}

void	Location::SaveExits(std::ofstream& file)
{
	bool no_moves(true);
	for(int count = 0;count < MAX_EXITS;++count)
	{
		if(exits[count] != NO_EXIT)
		{
			no_moves = false;
			break;
		}
	}

	if(no_moves)
	{
		if(no_exit != "")	// no moves but no_exit exists
			file << "    <exits";
		else
			return;
	}
	else
	{
		file << "    <exits";
		for(int count = 0;count < MAX_EXITS;++count)
		{
			if(exits[count] != NO_EXIT)
				file << " "<< directions[count] << "='" << exits[count] << "'";
		}
	}

	if(no_exit != "")
		file << " no-exit='" << no_exit << "'";
	file << "/>\n";
}

const std::string&	Location::SaveFlags()
{
	static std::string	ret;
	std::ostringstream	buffer;

	if(flags.any())
	{
		buffer << "flags='";
		if(flags.test(SPACE))		buffer << 's';
		if(flags.test(LINK))			buffer << 'l';
		if(flags.test(EXCHANGE))	buffer << 'e';
		if(flags.test(SHIPYARD))	buffer << 'y';
		if(flags.test(REPAIR))		buffer << 'r';
		if(flags.test(HOSPITAL))	buffer << 'h';
		if(flags.test(INSURE))		buffer << 'i';
		if(flags.test(PEACE))		buffer << 'p';
		if(flags.test(BAR))			buffer << 'b';
		if(flags.test(UNLIT))		buffer << 'u';
		if(flags.test(CUSTOM))		buffer << 'k';
		if(flags.test(TELEPORT))	buffer << 't';
		if(flags.test(COURIER))		buffer << 'c';
		if(flags.test(PICKUP))		buffer << 'a';
		if(flags.test(FIGHTING))	buffer << 'f';
		if(flags.test(WEAPONS))		buffer << 'w';
		buffer << "'";
	}

	ret = buffer.str();
	return ret;
}

void	Location::SaveName(std::ofstream& file)
{
	file << "    <name>" << EscapeXML(name) << "</name>\n";
}

void	Location::SaveVocab(std::ofstream& file)
{
	if(vocab.size() > 0)
	{
		for(auto iter = vocab.begin();iter != vocab.end();++iter)
		{
			// Keep in this format - EscapeXML() is static
			file << "    <vocab cmd='" << EscapeXML(iter->first); 
			file << "' event='" << EscapeXML(iter->second) << "'/>\n";
		}
	}
}

void	Location::SetEvent(int which,std::string event)
{
	Strip(event);
	events[which] = event;
}

void	Location::SetFlag(int which,bool set_to)
{
	if(which >= MAX_FLAGS)
		return;
	if(set_to)
		flags.set(which);
	else
		flags.reset(which);
}

void	Location::SetFlags(const std::string& flag_string)
{
	flags.reset();
	int len = flag_string.length();
	for(int count = 0;count < len;++count)
	{
		switch(flag_string[count])
		{
			case 's':	flags.set(SPACE);		break;
			case 'l':	flags.set(LINK);		break;
			case 'e':	flags.set(EXCHANGE);	break;
			case 'y':	flags.set(SHIPYARD);	break;
			case 'r':	flags.set(REPAIR);	break;
			case 'h':	flags.set(HOSPITAL);	break;
			case 'i':	flags.set(INSURE);	break;
			case 'p':	flags.set(PEACE);		break;
			case 'b':	flags.set(BAR);		break;
			case 'u':	flags.set(UNLIT);		break;
			case 'k':	flags.set(CUSTOM);	break;
			case 't':	flags.set(TELEPORT);	break;
			case 'c':	flags.set(COURIER);	break;
			case 'a':	flags.set(PICKUP);	break;
			case 'f':	flags.set(FIGHTING);	break;
			case 'w':	flags.set(WEAPONS);	break;
		}
	}
}

void	Location::SetName(std::string loc_name)
{
	Strip(loc_name);
	name = loc_name;
}

void	Location::SetNoExit(std::string loc_no_exit)
{
	Strip(loc_no_exit);
	no_exit = loc_no_exit;
}

std::string&	Location::Strip(std::string& text)
{
	for(int count = text.length() - 1;count >= 0;--count)
	{
		if((text[count] == ' ') || (text[count] == '\n') || (text[count] == '\r'))
			text.erase(count);
		else
			break;
	}
	return(text);
}

void	Location::WriteDescListing(LocEd *main_window)
{
	for(auto iter = desc.begin();iter != desc.end();++iter)
		main_window->DisplayListing(*iter);
}

void	Location::WriteEventsListing(LocEd *main_window)
{
	bool no_events(true);
	for(int count = 0;count < MAX_EVENTS;++count)
	{
		if(events[count] != "")
		{
			no_events = false;
			break;
		}
	}
	
	if(no_events)
	{
		main_window->DisplayListing(" ");
		return;
	}

	std::ostringstream	buffer;
	main_window->DisplayListing("Events:");
	for(int count = 0;count < MAX_EVENTS;++count)
	{
		if(events[count] != "")
		{
			buffer.str("");
			buffer << "  " << list_triggers[count] << ": " << events[count];
			main_window->DisplayListing(buffer.str());
		}
	}
	main_window->DisplayListing(" ");
}

void	Location::WriteExitsListing(LocEd *main_window)
{
	bool no_moves(true);
	for(int count = 0;count < MAX_EXITS;++count)
	{
		if(exits[count] != NO_EXIT)
		{
			no_moves = false;
			break;
		}
	}

	if(no_moves)
		return;

	std::ostringstream buffer;
	buffer << "Exits:";
	for(int count = 0;count < MAX_EXITS;++count)
	{
		if(exits[count] != NO_EXIT)
			buffer << "  "<< list_directions[count] << "=" << exits[count];
	}
	main_window->DisplayListing(buffer.str());
	
	if(no_exit != "")
	{
		buffer.str("");
		buffer << "No exit message: " << no_exit;
		main_window->DisplayListing(buffer.str());
	}
}

void	Location::WriteFlagsListing(LocEd *main_window)
{
	if(flags.any())
	{
		std::ostringstream	buffer;
		buffer << "Flags: ";
		if(flags.test(SPACE))		buffer << "space  ";
		if(flags.test(LINK))			buffer << "link  ";
		if(flags.test(EXCHANGE))	buffer << "exchange  ";
		if(flags.test(SHIPYARD))	buffer << "shipyard  ";
		if(flags.test(REPAIR))		buffer << "repairs  ";
		if(flags.test(HOSPITAL))	buffer << "hospital  ";
		if(flags.test(INSURE))		buffer << "insurance  ";
		if(flags.test(PEACE))		buffer << "peace  ";
		if(flags.test(BAR))			buffer << "bar  ";
		if(flags.test(UNLIT))		buffer << "unlit  ";
		if(flags.test(CUSTOM))		buffer << "custom  ";
		if(flags.test(TELEPORT))	buffer << "teleport  ";
		if(flags.test(COURIER))		buffer << "courier  ";
		if(flags.test(PICKUP))		buffer << "pickup  ";
		if(flags.test(FIGHTING))	buffer << "fighting  ";
		if(flags.test(WEAPONS))		buffer << "weapons  ";

		main_window->DisplayListing(buffer.str());
	}
}

void	Location::WriteListing()
{
	LocEd	*main_window = LocEd::GetMainWindow();
	std::ostringstream buffer;
	buffer << loc_no << ". " << name;
	main_window->DisplayListingBold(buffer.str());
	WriteDescListing(main_window);
	WriteFlagsListing(main_window);
	WriteExitsListing(main_window);
	WriteEventsListing(main_window);	
	WriteVocabListing(main_window);
//	main_window->DisplayListing(" ");
}

void	Location::WriteVocabListing(LocEd *main_window)
{
	std::ostringstream	buffer;
	if(vocab.size() > 0)
	{
		buffer << "Vocabulary:";
		main_window->DisplayListing(buffer.str());
		buffer.str("");
		for(auto iter = vocab.begin();iter != vocab.end();++iter)
		{
			buffer << "  cmd = " << iter->first << "   event = " << iter->second;
			main_window->DisplayListing(buffer.str());
		}
	}
}

