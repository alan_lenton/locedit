/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef LOCATION_H
#define LOCATION_H

#include	<array>
#include	<bitset>
#include <fstream>
#include <list>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <utility>

typedef	std::list<std::string>	ParaList;
typedef	std::map<std::string,std::string,std::less<std::string>>	Vocab;
typedef	std::unique_ptr<std::list<std::pair<int,int>>>	PtrExits;

class LocEd;
class MapParser;

class	Location
{
	friend class MapParser;

public:
	enum	{ NORTH,NE,EAST,SE,SOUTH,SW,WEST,NW,UP,DOWN,INTO,OUTOF,MAX_EXITS };
	enum	{ ENTER,NOEXIT,INROOM,SEARCH,MAX_EVENTS };
	enum
	{
		SPACE, LINK, EXCHANGE, SHIPYARD, REPAIR, HOSPITAL, INSURE, PEACE,
		BAR, COURIER, UNLIT, CUSTOM, TELEPORT, PICKUP, FIGHTING, WEAPONS,
		MAX_FLAGS
	};

	static const int	MAX_NAME = 64;
	static const int	MAX_DESC = 1024;
	static const int	MAX_EVENT_SZ = 40;
	static const int	MAX_MSSG = 40;
	static const int	INVALID_LOC = -1;
	static const int	COPIED_LOC = -2;
	static const int	NO_EXIT = -1;

private:
	static const std::string	triggers[];
	static const std::string	directions[];
	static const std::string	list_triggers[];
	static const std::string	list_directions[];

	int			loc_no;
	std::string	name;
	ParaList		desc;
	std::string	no_exit;
	Vocab			vocab;

	std::array<std::string,MAX_EVENTS>	events;
	std::array<int,MAX_EXITS>	exits;
	std::bitset<MAX_FLAGS>	flags;

	std::string&			CleanText(std::string& text);
	const std::string&	SaveFlags();

	bool	HasFlags()										{ return flags.any();		}

	void	Clear();
	void	SaveDesc(std::ofstream& file);
	void	SaveEvents(std::ofstream& file);
	void	SaveExits(std::ofstream& file);
	void	SaveName(std::ofstream& file);
	void	SaveVocab(std::ofstream& file);
	void	SetFlags(const std::string& flag_string);
	void	WriteDescListing(LocEd *mainWindow);
	void	WriteEventsListing(LocEd *main_window);
	void	WriteExitsListing(LocEd *main_window);
	void	WriteFlagsListing(LocEd *main_window);
	void	WriteVocabListing(LocEd *main_window);

	Location& operator=(const Location&);

public:
	static const std::string&	EscapeXML(const std::string& text);
	static std::string&	Strip(std::string& text);

	Location();
	Location(int loc_num);
	Location(const Location& Rhs);
	~Location()	{	}

	// Note that we want copies of the strings

	PtrExits	GetExits();
	std::string		GetName() const					{ return name;					}
	std::string		GetDesc() const;
	std::string		GetNoExit()	const					{ return no_exit;				}
	const Vocab&	GetVocab() const					{ return vocab;				}
	std::string GetEvent(int which)					{ return events[which];		}
	int	GetExit(int which)							{ return exits[which];		}
	int	Number()											{ return loc_no;				}
	
	bool	FlagIsSet(int which)							{ return flags.test(which);}
	bool	HasSubString(const std::string& text);

	void	AddDesc(std::string loc_desc,bool clear_first = true);
	void	AddName(std::string& loc_name)			{ name = loc_name;			}
	void	AddVocab(const std::string cmd,const std::string event);
	void	ClearDesc()										{ desc.clear();				}
	void	ClearExit(int dir)							{ exits[dir] = NO_EXIT;		}
	void	ClearVocab()									{ vocab.clear();				}
	void	DisplayDesc(LocEd	*main_window);
	void	DisplayEvents(LocEd	*main_window);
	void	DisplayExits(LocEd	*main_window);
	void	DisplayFlags(LocEd *main_window);
	void	DisplayLocNo(LocEd *main_window);
	void	DisplayName(LocEd	*main_window);
	void	DisplayVocab(LocEd *main_window);
	void	SetEvent(int which,std::string event);
	void	SetExit(int which,int where_to)			{ exits[which] = where_to;	}
	void	SetFlag(int which,bool set_to);
	void	SetLocNo(int new_loc_no)					{ loc_no = new_loc_no;		}
	void	SetName(std::string loc_name);
	void	SetNoExit(std::string loc_no_exit);

	void	Save(std::ofstream& file);	
	void	WriteListing();

};
#endif

