/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef MAPPARSER_H
#define MAPPARSER_H

#include <memory>

#include <QFile>
#include <QLabel>
#include <QString>

#include <QXmlDefaultHandler>


class Location;
class FedMap;

typedef std::shared_ptr<Location>	LocPtr;

class MapParser : public QXmlDefaultHandler
{
private:
	static const QString	element_names[];
	static const int		NOT_FOUND;

	FedMap	*fed_map;
	LocPtr	location;
	QFile		*file;
	QString	text;

	// These three functions are overrides of virtuals in QXmlDefaultHandler
	bool	characters(const QString& txt);
	bool	endElement(const QString& namespaceURI,const QString& localName,const QString& name);
	bool	startElement(const QString& namespaceURI,const QString& localName,
														const QString& qName, const QXmlAttributes& atts);

	int	FindElement(const QString& element);

	void	EndDesc();
	void	EndLocation();
	void	EndLocName();
	void	StartDesc(const QXmlAttributes& attribs);
	void	StartEvents(const QXmlAttributes& attribs);
	void	StartExits(const QXmlAttributes& attribs);
	void	StartFed2Map(const QXmlAttributes& attribs);
	void	StartLocation(const QXmlAttributes& attribs);
	void	StartLocName(const QXmlAttributes& attribs);
	void	StartVocab(const QXmlAttributes& attribs);

public:
	static QString&	EscapeXML(QString& text);

	MapParser(const std::string file_name, FedMap *the_map);
	~MapParser();

	bool	Parse();
};

#endif