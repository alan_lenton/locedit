/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "mapparser.h"

#include <stdexcept>

#include <QXmlInputSource>
#include <QXmlSimpleReader>

#include "fedmap.h"
#include "location.h"

// I ~think~ that the XML reader handles unescaping characters...

const QString	MapParser::element_names[] = 
			{ "fed2-map", "location", "name", "desc", "exits", "events", "vocab", "" };

const int		MapParser::NOT_FOUND = 999;

MapParser::MapParser(const std::string file_name, FedMap *the_map)
{
	fed_map = the_map;
	location = nullptr;

	file = new QFile(QString::fromStdString(file_name));
	if(!file->open(QIODevice::ReadOnly | QIODevice::Text))
	{
		file = nullptr;
		throw std::domain_error("file error");
	}
}

MapParser::~MapParser()
{
	file->close();
	delete file;
}


// These three functions are overrides of virtuals in QXmlDefaultHandler
bool	MapParser::characters(const QString& txt)
{
	text += txt;
	return true;
}

bool	MapParser::endElement(const QString& namespaceURI,const QString& localName,const QString& name)
{
	switch(FindElement(name))
	{
		case 0:	break;
		case 1:	EndLocation();	break;
		case 2:	EndLocName();	break;
		case 3:	EndDesc();		break;
		case 4:	break;	// End exits
		case 5:	break;	// End events
		case 6:	break;	// End vocab
	}
	return true;
}

bool	MapParser::startElement(const QString& namespaceURI,const QString& localName,
										const QString& qName, const QXmlAttributes& atts)
{
	switch(FindElement(qName))
	{
		case 0:	StartFed2Map(atts);	break;
		case 1:	StartLocation(atts);	break;
		case 2:	StartLocName(atts);	break;
		case 3:	StartDesc(atts);		break;
		case 4:	StartExits(atts);		break;
		case 5:	StartEvents(atts);	break;
		case 6:	StartVocab(atts);		break;
	}
	return true;
}


// Regular functions start here
void	MapParser::EndDesc()
{
	if(!text.isEmpty())
	{

		location->AddDesc(text.toStdString(),false);
		text = "";
	}
}

void	MapParser::EndLocation()
{
	if(location != nullptr)	
	{
		fed_map->AddLocation(location);
		location = nullptr;
	}
}

void	MapParser::EndLocName()
{
	if(!text.isEmpty())
	{
		location->SetName(text.toStdString());
		text = "";
	}
}

int	MapParser::FindElement(const QString& element)
{
	for(int which = 0;element_names[which] != "";++which)
	{
		if(element == element_names[which])
			return which;
	}
	return NOT_FOUND;
}

bool	MapParser::Parse()
{
	QXmlInputSource	source(file);
	QXmlSimpleReader	reader;
	reader.setContentHandler(this);
	return reader.parse(source);
}

void	MapParser::StartDesc(const QXmlAttributes& attribs)
{
	text = "";
}

void	MapParser::StartEvents(const QXmlAttributes& attribs)
{
	static const QString	events[] = { "enter","no-exit","in-room","search","" };

	if(location == nullptr)
		return;

	QString	event;
	for(int count = 0;events[count] != "";++count)
	{
		event = attribs.value(events[count]);
		if(!event.isEmpty())
			location->SetEvent(count,event.toStdString());
	}
}

void	MapParser::StartExits(const QXmlAttributes& attribs)
{
	if(location == nullptr)
		return;

	QString	where_to;
	for(int count = 0;count < Location::MAX_EXITS;++count)
	{
		where_to = attribs.value(QString::fromLatin1(Location::directions[count].c_str()));
		if(!where_to.isEmpty())
			location->SetExit(count,where_to.toInt());
	}
	QString	no_exit = attribs.value(QString::fromLatin1("no-exit"));
	if(!no_exit.isEmpty())
		location->no_exit = no_exit.toStdString();
}

void	MapParser::StartFed2Map(const QXmlAttributes& attribs)
{
	fed_map->title = attribs.value(QString::fromLatin1("title")).toStdString();
	fed_map->version = attribs.value(QString::fromLatin1("version")).toInt();
	if(fed_map->version < 1)
		fed_map->version = 1;
	fed_map->launch_loc = attribs.value(QString::fromLatin1("from")).toStdString();
	fed_map->orbit_loc = attribs.value(QString::fromLatin1("to")).toStdString();
}

void	MapParser::StartLocation(const QXmlAttributes& attribs)
{
	int	loc_no = attribs.value(QString::fromLatin1("num")).toInt();
	if(fed_map->IsOffMap(loc_no))
	{
		location = nullptr;
		return;
	}

	location.reset(new Location(loc_no));
	QString	flags = attribs.value(QString::fromLatin1("flags"));
	if(!flags.isEmpty())
	{
		location->SetFlags(flags.toStdString());
		if(location->FlagIsSet(Location::SPACE))
			fed_map->is_a_space_map = true;
	}
}

void	MapParser::StartLocName(const QXmlAttributes& attribs)
{
	text = "";
}

void	MapParser::StartVocab(const QXmlAttributes& attribs)
{
	if(location == nullptr)
		return;

	QString	cmd = attribs.value(QString::fromLatin1("cmd"));
	QString	event = attribs.value(QString::fromLatin1("event"));
	location->AddVocab(cmd.toStdString(),event.toStdString());
}

