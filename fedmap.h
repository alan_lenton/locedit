/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef LOCMAP_H
#define LOCMAP_H

#include <fstream>
#include <list>
#include	<map>
#include <memory>
#include <string>

class	Location;
class MapParser;
class	MapScene;

typedef std::shared_ptr<Location>	LocPtr;
typedef std::map<int,LocPtr>			LocIndex;

class	FedMap
{
	friend class MapParser;

public:
	static LocPtr		clip_board;

	static const int	HEIGHT = 64;
	static const int	WIDTH = 64;
	static const int	OFF_MAP = -1;
	static const int	NOT_A_LOC = -2;
	static const int	NOT_A_LINK = -3;
	static const int	WRAP = -4;
	static const int	CURRENT_LOC = -99;

	static int	LocNumber(int col,int row);
	static int	Adjacent(int dir,int loc_no);

	static bool	IsAPossibleLink(int col,int row);
	static bool	IsAPossibleLoc(int col,int row);
	static bool IsOffMap(int loc_no);
	static bool	IsOffMap(int col,int row);

	enum	{ SPACE_MAP, PLANET_MAP };	// Type of map
	enum	{ NORMAL, NON_ADJACENT };	// state of mvt buttons

private:
	std::string	file_name;
	std::string	title;
	std::string	launch_loc;
	std::string	orbit_loc;
	LocIndex		loc_index;

	int	version;
	int	current_selection;
	bool	is_a_space_map;
	bool	mvt_state;
	bool	is_modified;

	LocPtr	unmodified_edit_loc;
	MapScene	*scene;

	LocPtr	Find(int loc_no);
	LocPtr	NewLocation(int loc_no);

	int	OppositeCardinalDirection(int direction);

	bool	AddMvtArrows(int from,int direction);

	void	ClearLocationDisplay();
	void	DeselectLocTile(int loc_no,LocPtr loc);
	void	DisplayInfo(LocPtr loc);
	void	UpdateLocTile(int loc_no,LocPtr loc);

public:
	static std::string& SanitizeStdString(std::string& title_string);
	static std::string& SanitizeTitle(std::string& title_string);

	FedMap();	
	~FedMap()	{	}

	std::unique_ptr<std::list<std::pair<int,int>>>	GetExits(int loc_no);

	const std::string&	GetLaunchLoc()		{ return launch_loc;				}
	const std::string&	GetOrbitLoc()		{ return orbit_loc;				}

	std::string				GetFileName()		{ return file_name;				}
	std::string				GetTitle()			{ return title;					}

	int	CurrentLocNo()							{ return current_selection;	}
	int	FindMvtTile(int from, int direction);
	int	GetExit(int loc_no, int which);
	int	GetMvtState()							{ return mvt_state;				}

	bool	CheckMap();
	bool	CopyLoc();
	bool	CutLoc();
	bool	IsASpaceMap()							{ return is_a_space_map;		}
	bool	IsModified()							{ return is_modified;			}
	bool	SetExit(int dir,int to,int loc_no);
	bool	SetFlag(int which,int loc_no,bool value);
	bool	ToggleExit(int dir,int loc_no);

	void	AddDesc(std::string& text,int loc_no);
	void	AddLocation(LocPtr loc);
	void	AddName(std::string name,int loc_no = CURRENT_LOC);
	void	AddVocab(int loc_no);
   void	AddVocab(std::string cmd,std::string event,int loc_no);
	void	ClearAllSpaceFlags();
	void	ClearAllPlanetFlags();
	void	ClearDesc(int loc_no);
	void	ClearExit(int dir,int loc_no);
	void	ClearLocs();
	void	ClearModStatus()						{ is_modified = false;			}
	void	ClearVocab(int loc_no);
	void	ClickAt(int abs_row,int abs_column);
	void	DeleteLoc();
	void	DisplayLocInfo(int loc_no);
	void	DisplayMapInfo();
	void	FindNoSpaceFlag();
	void	FindProperty(int flag);
	void	FindText(const std::string& text);
	void	InitializeNewMap();	
	void	Load(const std::string& new_file_name);
	void	MakeEditLoc(int loc_no);
	void	MouseAt(int abs_row,int abs_column);
	void	PasteLoc();
	void	ReloadLoc();
	void	Save(bool do_map_check);
	void	SaveAs(const std::string new_name);
	void	SetAllSpaceFlags();
	void	SetDesc(std::string text);
	void	SetEvent(int which,std::string event);
	void	SetExit(int which,int loc_no);
	void	SetLaunch(std::string text);
	void	SetMapType(int map_type);
	void	SetModified()							{ is_modified = true;		}
	void	SetMvtState(int new_state)			{ mvt_state = new_state;	}
	void	SetNoExit(std::string loc_no_exit);	
	void	SetOrbit(std::string text);
	void	SetScene(MapScene *map_scene)		{ scene = map_scene;			}
	void	SetTitle(std::string text);
	void	UpdateScene();
	void	UpdateScene(int loc_no);
	void	WriteListing() const;
};

#endif

