/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

// General Note: Don't use shared pointers for Qt stuff, unless you want 
// to get your knickers in a twist 

#include "newmap.h"

// #include <QMessageBox>

#include "fedmap.h"

NewMap::NewMap(QWidget *parent)	: QDialog(parent)
{
	ui.setupUi(this);
	space_map = false;
	ui.okButton->setEnabled(false);

	connect(ui.okButton,SIGNAL(clicked()),this,SLOT(okButtonSlot()));
	connect(ui.system_edit,SIGNAL(textChanged(const QString&)),this,SLOT(SystemNameChangedSlot(const QString&)));
	connect(ui.title_edit,SIGNAL(textChanged(const QString&)),this,SLOT(TitleChangedSlot(const QString&)));
	connect(ui.space_radio,SIGNAL(toggled(bool)),this,SLOT(SpaceBtnToggledSlot(bool)));
}


void	NewMap::okButtonSlot()
{
	if(ui.space_radio->isChecked())
	{
		if(ui.system_edit->text() == "")
			ui.title_edit->clear();
		else
		{
			FedMap::SanitizeTitle(ui.system_edit->text().toStdString());
			ui.title_edit->setText(ui.system_edit->text() + QString(" Space"));
		}
	}

	system = ui.system_edit->text();
	title = ui.title_edit->text(); 
	space_map = ui.space_radio->isChecked();
	accepted();
}

void	NewMap::SpaceBtnToggledSlot(bool checked)
{
	if(checked)
	{
		if(ui.system_edit->text() == "")
			ui.title_edit->clear();
		else
			ui.title_edit->setText(ui.system_edit->text() + QString(" Space"));
		ui.title_edit->setEnabled(false);
		ui.map_name_label->setText("Map Title");
	}
	else
	{
		ui.title_edit->clear();
		ui.title_edit->setEnabled(true);
		ui.map_name_label->setText("Planet Name");
	}
}

void	NewMap::SystemNameChangedSlot(const QString& new_text)
{
	if(new_text == "")
	{
		ui.title_edit->clear();
		ui.okButton->setEnabled(false);
	}
	else
	{
		ui.system_edit->setText(QString::fromStdString(FedMap::SanitizeTitle(ui.system_edit->text().toStdString())));
		if(ui.space_radio->isChecked())
			ui.title_edit->setText(ui.system_edit->text() + QString(" Space"));
		ui.okButton->setEnabled(true);
	}
}

void	NewMap::TitleChangedSlot(const QString& new_text)
{
	ui.title_edit->setText(QString::fromStdString(FedMap::SanitizeTitle(ui.title_edit->text().toStdString())));
}

