/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef FINDPROPERTY_H
#define FINDPROPERTY_H


#include "ui_findproperty.h"

class	FindProperty : public QDialog
{
	Q_OBJECT

public:
	static const int	NOT_SPACE = -1;
	static const int	NOT_FOUND = -2;

private:
	Ui::FindPropertyDialog	ui;

public:
	FindProperty(QWidget *parent);

	int	FlagToCheck();
};

#endif

