/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2014 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Program compiled with Visual Studio 2010 and Qt4.8.4
	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

// General Note: Don't use shared pointers for Qt stuff, unless you want 
// to get your knickers in a twist 

#include "loced.h"

#include <QDir>
#include <QColorDialog>
#include <QFileDialog>
#include <QFontDialog>
#include <QMessageBox>
#include <QSettings>
#include <QSplitter>
#include	<QString>
#include <QIntValidator>

#include "about.h"
#include "fedmap.h"
#include "findproperty.h"
#include "findtext.h"
#include "location.h"
#include "newmap.h"
#include "scene.h"
#include "tiles.h"

LocEd	*LocEd::main_window = nullptr;

LocEd::LocEd(QWidget *parent, Qt::WindowFlags flags) : QMainWindow(parent, flags)
{
	ui.setupUi(this);

	version = "2.73";

	ui.splitter->setChildrenCollapsible(false);

	ui.progress_bar->setMaximum(MAX_DESC_CHARS);
	ui.progress_bar->setMinimum(0);

	fed_map = new FedMap();
	scene = new MapScene(fed_map);
	ui.graphics_view->setScene(scene);
	ui.graphics_view->ensureVisible(0,0,16,16);
	fed_map->SetScene(scene);
	system_name = "";

	LoadSettings();
	QStringList	headers;
	headers.append("Command");
	headers.append("Event");
	ui.cmd_table->setHorizontalHeaderLabels(headers);
	int row_count =  ui.cmd_table->rowCount();
	for(int count = 0;count < row_count;++count)
		ui.cmd_table->setRowHeight(count,20);

	QString title("Location Editor - Federation II - Version ");
	title += version;
	setWindowTitle(title);

	QCoreApplication::setOrganizationName("ibgames");
	QCoreApplication::setOrganizationDomain("ibgames.com");
	QCoreApplication::setApplicationName("LocEd");


	planet_image = QImage(":/images/planet.png");
	space_image = QImage(":/images/space.png");
	ui.map_type_canvas->installEventFilter(this);

	last_arrow_selected = NO_SELECTION;
	adj_selected = false;
	statusBar()->addPermanentWidget(ui.loc_label);
	statusBar()->addPermanentWidget(ui.loc_no_label);
	statusBar()->addPermanentWidget(ui.modified_label);
	statusBar()->addWidget(ui.spacer_label);
	statusBar()->addWidget(ui.file_label);

	SetUpConnections();
	SetMvtValidator();
	main_window = this;
}

LocEd::~LocEd()
{

}


void	LocEd::AboutActionSlot()		
{ 
	About	about(this,version);
	about.exec();
}

void	LocEd::AddToMiscText(const std::string& text)
{
	ui.info_tab->setCurrentIndex(MAP_INFO);
	ui.misc_text_edit->appendPlainText(QString::fromStdString(text));
}

void	LocEd::ArrowsOff()
{
	ui.down_btn->setEnabled(false);
	ui.east_btn->setEnabled(false);
	ui.in_btn->setEnabled(false);
	ui.ne_btn->setEnabled(false);
	ui.north_btn->setEnabled(false);
	ui.not_adj_btn->setEnabled(false);
	ui.nw_btn->setEnabled(false);
	ui.out_btn->setEnabled(false);
	ui.se_btn->setEnabled(false);
	ui.south_btn->setEnabled(false);
	ui.sw_btn->setEnabled(false);
	ui.up_btn->setEnabled(false);
	ui.west_btn->setEnabled(false);
}

void	LocEd::ArrowsOn()
{
	ui.down_btn->setEnabled(true);
	ui.east_btn->setEnabled(true);
	ui.in_btn->setEnabled(true);
	ui.ne_btn->setEnabled(true);
	ui.north_btn->setEnabled(true);
	ui.not_adj_btn->setEnabled(false);
	ui.nw_btn->setEnabled(true);
	ui.out_btn->setEnabled(true);
	ui.se_btn->setEnabled(true);
	ui.south_btn->setEnabled(true);
	ui.sw_btn->setEnabled(true);
	ui.up_btn->setEnabled(true);
	ui.west_btn->setEnabled(true);
}

void	LocEd::BarCheckSlot(bool state)	{ fed_map->SetFlag(Location::BAR,FedMap::CURRENT_LOC,state); }

void	LocEd::CanEdit(bool allow)
{
	ui.name_edit->setEnabled(allow);
	ui.desc_edit->setEnabled(allow);
	ui.north_text_edit->setEnabled(allow);
	ui.south_text_edit->setEnabled(allow);
	ui.east_text_edit->setEnabled(allow);
	ui.west_text_edit->setEnabled(allow);
	ui.ne_text_edit->setEnabled(allow);
	ui.nw_text_edit->setEnabled(allow);
	ui.se_text_edit->setEnabled(allow);
	ui.sw_text_edit->setEnabled(allow);
	ui.up_text_edit->setEnabled(allow);
	ui.down_text_edit->setEnabled(allow);
	ui.in_text_edit->setEnabled(allow);
	ui.out_text_edit->setEnabled(allow);
	ui.no_exit_edit->setEnabled(allow);
	ui.enter_edit->setEnabled(allow);
	ui.in_room_edit->setEnabled(allow);
	ui.no_move_edit->setEnabled(allow);
	ui.search_edit->setEnabled(allow);
}

bool	LocEd::CheckForSave()
{
	bool modified = fed_map->IsModified();
	if(modified == false)
		return true;

	QMessageBox::StandardButton result = QMessageBox::warning(this,tr("Unsaved changes"),
								tr("There are unsaved changes to the map\nWould you like them saving?"),
								QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

	switch(result)
	{
		case QMessageBox::Yes:	SaveActionSlot();	return true;
		case QMessageBox::No:							return true;
		case QMessageBox::Cancel:						return false;
	}
	return	false;
}

void	LocEd::ClearEvents()
{
	ui.enter_edit->clear();
	ui.enter_edit->setToolTip("");
	ui.in_room_edit->clear();
	ui.in_room_edit->setToolTip("");
	ui.no_move_edit->clear();
	ui.no_move_edit->setToolTip("");
	ui.search_edit->clear();
	ui.search_edit->setToolTip("");
}

void	LocEd::ClearExitListing()
{
	for(int count = Location::NORTH;count < Location::MAX_EXITS;++count)
	{
		switch(count)
		{
			case Location::NORTH:	ui.north_text_edit->clear();	ui.north_text_edit->setToolTip("");	break;
			case Location::NE:		ui.ne_text_edit->clear();		ui.ne_text_edit->setToolTip("");		break;
			case Location::EAST:		ui.east_text_edit->clear();	ui.east_text_edit->setToolTip("");	break;
			case Location::SE:		ui.se_text_edit->clear();		ui.se_text_edit->setToolTip("");		break;
			case Location::SOUTH:	ui.south_text_edit->clear();	ui.south_text_edit->setToolTip("");	break;
			case Location::SW:		ui.sw_text_edit->clear();		ui.sw_text_edit->setToolTip("");		break;
			case Location::WEST:		ui.west_text_edit->clear();	ui.west_text_edit->setToolTip("");	break;
			case Location::NW:		ui.nw_text_edit->clear();		ui.nw_text_edit->setToolTip("");		break;
			case Location::UP:		ui.up_text_edit->clear();		ui.up_text_edit->setToolTip("");		break;
			case Location::DOWN:		ui.down_text_edit->clear();	ui.down_text_edit->setToolTip("");	break;
			case Location::INTO:		ui.in_text_edit->clear();		ui.in_text_edit->setToolTip("");		break;
			case Location::OUTOF:	ui.out_text_edit->clear();		ui.out_text_edit->setToolTip("");	break;
		}
	}
	ui.no_exit_edit->clear();
	ui.no_exit_edit->setToolTip("");
}

void	LocEd::ClearLocation()
{
	ClearDesc();
	ClearEvents();
	ClearExitListing();
	ClearLocFlags();
	ClearLocName();
	ClearMiscEdit();
	ClearVocab();
}

void	LocEd::ClearLocFlags()
{
	ui.space_check->setChecked(false);
	ui.link_check->setChecked(false);
	ui.peace_check->setChecked(false);
	ui.shipyard_check->setChecked(false);
	ui.shiprepairs_check->setChecked(false);
	ui.hospital_check->setChecked(false);
	ui.exchange_check->setChecked(false);
	ui.bar_check->setChecked(false);
	ui.teleport_check->setChecked(false);
	ui.custom_check->setChecked(false);
	ui.courier_check->setChecked(false);
	ui.pickup_check->setChecked(false);
	ui.fighting_check->setChecked(false);
	ui.weapons_check->setChecked(false);

}

void	LocEd::ClearVocab()
{
	ui.cmd_table->clear();
	QStringList	headers;
	headers.append("Command");
	headers.append("Event");
	ui.cmd_table->setHorizontalHeaderLabels(headers);
}

void	LocEd::closeEvent(QCloseEvent *event)			
{
	if(CheckForSave())
	{
		SaveSettings();
		event->accept();
	}
	else
		event->ignore();
}

void	LocEd::CopyActionSlot()					
{
	fed_map->AddName(ui.name_edit->text().toStdString());
	fed_map->SetDesc(ui.desc_edit->toPlainText().toStdString());
	if(fed_map->CopyLoc())
		SetMenuLoc(true);	
}

void	LocEd::CourierCheckSlot(bool state)	{ fed_map->SetFlag(Location::COURIER,FedMap::CURRENT_LOC,state); }
void	LocEd::CustomCheckSlot(bool state)	{ fed_map->SetFlag(Location::CUSTOM,FedMap::CURRENT_LOC,state); }

void	LocEd::CutActionSlot()					
{ 
	fed_map->AddName(ui.name_edit->text().toStdString());
	fed_map->SetDesc(ui.desc_edit->toPlainText().toStdString());
	if(fed_map->CutLoc())
		SetMenuLoc(true);	
}

void	LocEd::DeleteActionSlot()
{ 
	QMessageBox::StandardButton result = QMessageBox::question(this,tr("Delete Location"),
														tr("Delete this location?\nAre you sure you want to do that?"), 
														QMessageBox::Ok | QMessageBox::Cancel);
	if(result == QMessageBox::Ok)
	{
		fed_map->DeleteLoc();
		UpdateLocLabel(tr("Location Deleted"));
	}
}

void	LocEd::DescTextChangedSlot()
{
	int char_count(ui.desc_edit->toPlainText().length());

	if(char_count > MAX_DESC_CHARS)
		ui.desc_edit->textCursor().deletePreviousChar();
	else
	{
		ui.lcd_number->display(char_count);
		ui.progress_bar->setValue(char_count);
	}
}

void	LocEd::DisableAllLocProperties()
{
	ui.link_check->setEnabled(false);
	ui.peace_check->setEnabled(false);
	ui.shipyard_check->setEnabled(false);
	ui.shiprepairs_check->setEnabled(false);
	ui.hospital_check->setEnabled(false);
	ui.exchange_check->setEnabled(false);
	ui.bar_check->setEnabled(false);
	ui.teleport_check->setEnabled(false);
	ui.courier_check->setEnabled(false);
	ui.pickup_check->setEnabled(false);
	ui.custom_check->setEnabled(false);
	ui.fighting_check->setEnabled(false);
	ui.weapons_check->setEnabled(false);

}

void	LocEd::DisableMvtArrows(int except)
{
	ui.north_btn->setEnabled(false);
	ui.ne_btn->setEnabled(false);
	ui.east_btn->setEnabled(false);
	ui.se_btn->setEnabled(false);
	ui.south_btn->setEnabled(false);
	ui.sw_btn->setEnabled(false);
	ui.west_btn->setEnabled(false);
	ui.nw_btn->setEnabled(false);
	ui.up_btn->setEnabled(false);
	ui.down_btn->setEnabled(false);
	ui.in_btn->setEnabled(false);
	ui.out_btn->setEnabled(false);
	ui.not_adj_btn->setEnabled(false);

	if(except != Location::INVALID_LOC)
	{
		switch(except)
		{
			case Location::UP:		ui.up_btn->setEnabled(true);			break;
			case Location::DOWN:		ui.down_btn->setEnabled(true);		break;
			case Location::INTO:		ui.in_btn->setEnabled(true);			break;
			case Location::OUTOF:	ui.out_btn->setEnabled(true);			break;
			case ADJ_BTN:				ui.not_adj_btn->setEnabled(true);	break;
		}
	}
}

void	LocEd::DisplayDesc(const std::string& para)	{ ui.desc_edit->appendPlainText(QString::fromStdString(para)); }

void	LocEd::DisplayEvent(int which,const std::string& event)
{
	switch(which)
	{
		case Location::ENTER:	ui.enter_edit->setText(QString::fromStdString(event));	
										ui.enter_edit->setToolTip(ui.enter_edit->text());
										break;
		case Location::NOEXIT:	ui.no_move_edit->setText(QString::fromStdString(event));	
										ui.no_move_edit->setToolTip(ui.no_move_edit->text());
										break;
		case Location::INROOM:	ui.in_room_edit->setText(QString::fromStdString(event));	
										ui.in_room_edit->setToolTip(ui.in_room_edit->text());
										break;
		case Location::SEARCH:	ui.search_edit->setText(QString::fromStdString(event));	
										ui.search_edit->setToolTip(ui.search_edit->text());
										break;
	}
}

void	LocEd::DisplayExit(int which,int where)
{
	switch(which)
	{
		case Location::NORTH:	ui.north_text_edit->setText(QString::number(where)); break;
		case Location::NE:		ui.ne_text_edit->setText(QString::number(where)); break;
		case Location::EAST:		ui.east_text_edit->setText(QString::number(where)); break;
		case Location::SE:		ui.se_text_edit->setText(QString::number(where)); break;
		case Location::SOUTH:	ui.south_text_edit->setText(QString::number(where)); break;
		case Location::SW:		ui.sw_text_edit->setText(QString::number(where)); break;
		case Location::WEST:		ui.west_text_edit->setText(QString::number(where)); break;
		case Location::NW:		ui.nw_text_edit->setText(QString::number(where)); break;
		case Location::UP:		ui.up_text_edit->setText(QString::number(where)); break;
		case Location::DOWN:		ui.down_text_edit->setText(QString::number(where)); break;
		case Location::INTO:		ui.in_text_edit->setText(QString::number(where)); break;
		case Location::OUTOF:	ui.out_text_edit->setText(QString::number(where)); break;
	}
}

void	LocEd::DisplayFileName(std::string text)				{ ui.file_label->setText(QString::fromStdString(text)); }
void	LocEd::DisplayLaunchLoc(const std::string& loc_no)	{ ui.launch_edit->setText(QString::fromStdString(loc_no)); }
void	LocEd::DisplayListing(std::string text)				{ ui.listing_edit->appendPlainText(QString::fromStdString(text)); }

void	LocEd::DisplayListingBold(std::string text,bool has_internal_format)
{
	if(has_internal_format)
		ui.listing_edit->appendHtml(QString::fromStdString(text));
	else
		ui.listing_edit->appendHtml((QString)("<b>") + QString::fromStdString(text) + "</b>");
}

void	LocEd::DisplayListing2StartPos()
{
	QTextCursor cursor = ui.listing_edit->textCursor();
	cursor.movePosition(QTextCursor::Start);
	ui.listing_edit->setTextCursor(cursor);
}

void	LocEd::DisplayLocFlag(int which)
{
	switch(which)
	{
		case Location::SPACE:		ui.space_check->setChecked(true);			break;
		case Location::LINK:			ui.link_check->setChecked(true);				break;
		case Location::EXCHANGE:	ui.exchange_check->setChecked(true);		break;
		case Location::SHIPYARD:	ui.shipyard_check->setChecked(true);		break;
		case Location::REPAIR:		ui.shiprepairs_check->setChecked(true);	break;
		case Location::HOSPITAL:	// drop through
		case Location::INSURE:		ui.hospital_check->setChecked(true);		break;
		case Location::PEACE:		ui.peace_check->setChecked(true);			break;
		case Location::BAR:			ui.bar_check->setChecked(true);				break;
		case Location::COURIER:		ui.courier_check->setChecked(true);			break;
		case Location::UNLIT:		break;
		case Location::CUSTOM:		ui.custom_check->setChecked(true);			break;
		case Location::TELEPORT:	ui.teleport_check->setChecked(true);		break;
		case Location::PICKUP:		ui.pickup_check->setChecked(true);			break;
		case Location::FIGHTING:	ui.fighting_check->setChecked(true);		break;
		case Location::WEAPONS:		ui.weapons_check->setChecked(true);			break;
	}
}

void	LocEd::DisplayLocNo(int loc_no)
{
	ui.loc_no_edit->clear();
	ui.loc_no_edit->setText(QString::number(loc_no));
}

void	LocEd::DisplayLocsUsed(int num_locs)					{ ui.locs_used_edit->setText(QString::number(num_locs)); }

void	LocEd::DisplayMapTitle(const std::string title)		
{ 
	ui.title_edit->setText(QString::fromStdString(title)); 
	ui.title_edit->setToolTip(ui.title_edit->text());
}

void	LocEd::DisplayMapType()
{
	if(IsSpaceMap())
		ui.map_type_label->setText("Space\nMap");
	else
		ui.map_type_label->setText("Planet\nMap");
	ui.map_type_canvas->update();
}

void	LocEd::DisplayName(const std::string name)			{ ui.name_edit->setText(QString::fromStdString(name)); }

void	LocEd::DisplayNoExit(const std::string& no_exit)	
{ 
	ui.no_exit_edit->setText(QString::fromStdString(no_exit)); 
	ui.no_exit_edit->setToolTip(ui.no_exit_edit->text());
}

void	LocEd::DisplayOrbitLoc(const std::string& loc_no)	
{ 
	QString orbit = QString::fromStdString(loc_no);
	QStringList	orbit_list = orbit.split(".",QString::SkipEmptyParts);
	if(orbit_list.size() > 0)
	{
		ui.system_edit->setText(orbit_list[0]);
		ui.system_edit->setToolTip(ui.system_edit->text());
		if(orbit_list.size() > 2)
		{
			ui.orbit_edit->setText(orbit_list[2]); 
			ui.orbit_edit->setToolTip(ui.orbit_edit->text());
		}
	}
}

void	LocEd::DisplaySystemName()
{
	QString	title = QString::fromStdString(fed_map->GetTitle());
	if(!title.endsWith("Space"))
		return;
	else
	{
		title.remove(" Space");
		ui.system_edit->setText(title);
	}
}

void	LocEd::DisplayVocab(const std::string& cmd,const std::string& event)
{
	int row_count =  ui.cmd_table->rowCount();
	for(int count = 0;count < row_count;++count)
	{
		if(ui.cmd_table->itemAt(count,0) == 0)
		{
			QTableWidgetItem *new_cmd = new QTableWidgetItem(QString::fromStdString(cmd));
			ui.cmd_table->setItem(count,0,new_cmd);
			QTableWidgetItem *new_event = new QTableWidgetItem(QString::fromStdString(event));
			ui.cmd_table->setItem(count,1,new_event);
			return;
		}
	}

	// Hmm - must need to add some more rows!
	ui.cmd_table->setRowCount(ui.cmd_table->rowCount() + 10);
	DisplayVocab(cmd,event);
}

void	LocEd::DownBtnSlot()											{ ProcessDetatchedMove(Location::DOWN); }
void	LocEd::DownCompleteSlot()									{ fed_map->SetExit(Location::DOWN,ui.down_text_edit->text().toInt()); }
void	LocEd::EastBtnSlot()											{ ProcessArrowClick(Location::EAST); }
void	LocEd::EastCompleteSlot()									{ fed_map->SetExit(Location::EAST,ui.east_text_edit->text().toInt()); }

void	LocEd::EnableEditor()
{
	ui.map_tab->setEnabled(true);
	ui.info_tab->setEnabled(true);
	ui.space_check->setEnabled(false);
	ui.desc_frame->setEnabled(true);
	ui.misc_text_edit->setEnabled(true);

	ui.north_text_edit->setEnabled(true);
	ui.south_text_edit->setEnabled(true);
	ui.east_text_edit->setEnabled(true);
	ui.west_text_edit->setEnabled(true);
	ui.ne_text_edit->setEnabled(true);
	ui.se_text_edit->setEnabled(true);
	ui.sw_text_edit->setEnabled(true);
	ui.nw_text_edit->setEnabled(true);
	ui.up_text_edit->setEnabled(true);
	ui.down_text_edit->setEnabled(true);
	ui.in_text_edit->setEnabled(true);
	ui.out_text_edit->setEnabled(true);
	ui.no_exit_edit->setEnabled(true);

	ui.enter_edit->setEnabled(true);
	ui.in_room_edit->setEnabled(true);
	ui.no_move_edit->setEnabled(true);
	ui.search_edit->setEnabled(true);

	ui.save_action->setEnabled(true);
	ui.save_as_action->setEnabled(true);
	ui.find_text_action->setEnabled(true);
	ui.find_property_action->setEnabled(true);
}

void	LocEd::EnableMvtArrows()
{
	ui.north_btn->setEnabled(true);
	ui.ne_btn->setEnabled(true);
	ui.east_btn->setEnabled(true);
	ui.se_btn->setEnabled(true);
	ui.south_btn->setEnabled(true);
	ui.sw_btn->setEnabled(true);
	ui.west_btn->setEnabled(true);
	ui.nw_btn->setEnabled(true);
	ui.up_btn->setEnabled(true);
	ui.down_btn->setEnabled(true);
	ui.in_btn->setEnabled(true);
	ui.out_btn->setEnabled(true);
}

void	LocEd::EnterEventSlot()	{ fed_map->SetEvent(Location::ENTER,ui.enter_edit->text().toStdString()); }

bool LocEd::eventFilter(QObject *target,QEvent *event)
{
	if((target == ui.map_type_canvas) && (event->type() == QEvent::Paint))
	{
		QPainter	painter(ui.map_type_canvas);
		if(IsSpaceMap())
			painter.drawImage(0,0,space_image);
		else
			painter.drawImage(0,0,planet_image);
		return(true);
	}
	return(QWidget::eventFilter(target,event));
}

void	LocEd::ExchangeCheckSlot(bool state)	
{ 
	if(!fed_map->SetFlag(Location::EXCHANGE,FedMap::CURRENT_LOC,state))
		ui.exchange_check->setChecked(false); 
}

void	LocEd::ExitActionSlot()						{ close(); }

QString	LocEd::ExtractDataDir(QString file_path)
{
	int index = file_path.lastIndexOf('/');
	if(index == -1)
		return "./";
	
	QString	data_dir(file_path);
	data_dir.truncate(index);
	return data_dir;
}

void	LocEd::FightingCheckSlot(bool state)	
{ 
	if(!fed_map->SetFlag(Location::FIGHTING,FedMap::CURRENT_LOC,state))
		ui.fighting_check->setChecked(false);  
}

void	LocEd::FindPropertySlot()
{
	FindProperty	find_property(this);
	if(find_property.exec() == QDialog::Accepted)
	{
		int flag = find_property.FlagToCheck();
		if(flag == FindProperty::NOT_FOUND)
			return;
		if(flag == FindProperty::NOT_SPACE)
			fed_map->FindNoSpaceFlag(); 
		else
			fed_map->FindProperty(flag);
	}
}

void	LocEd::FindTextSlot()
{
	FindText	find_text(this);
	if(find_text.exec() == QDialog::Accepted)
	{
		QString	text(find_text.GetText());
		if(text != "")
			fed_map->FindText(text.toStdString());
	}
}

void	LocEd::FontActionSlot()			
{
	bool	ok;
	QFont font = QFontDialog::getFont(&ok,ui.desc_edit->font(),this);
	if(ok)
	{
		ui.name_edit->setFont(font);
		ui.desc_edit->setFont(font);
		ui.listing_edit->setFont(font);
	}
}

std::string LocEd::GetSaveFileName()
{
	return(QFileDialog::getSaveFileName(this, tr("Save File"),"./",tr("Fed Map (*.loc)")).toStdString());
}

void	LocEd::GetVocab(std::map<std::string,std::string>& vocab)
{
	for(auto count = 0;;count++)
	{
		auto cmd = ui.cmd_table->item(count,0);
		if(cmd == nullptr)
			break;
		auto event = ui.cmd_table->item(count,1);
		if(event == nullptr)
			continue;

		auto cmd_string = cmd->data(Qt::EditRole).toString().toStdString();
		auto event_string = event->data(Qt::EditRole).toString().toStdString();
		vocab[cmd_string] = event_string;
	}
}

void	LocEd::HospitalCheckSlot(bool state)	{ fed_map->SetFlag(Location::HOSPITAL,FedMap::CURRENT_LOC,state); }
void	LocEd::InBtnSlot()							{ ProcessDetatchedMove(Location::INTO); }
void	LocEd::InCompleteSlot()						{ fed_map->SetExit(Location::INTO,ui.in_text_edit->text().toInt()); }
void	LocEd::InRoomEventSlot()					{ fed_map->SetEvent(Location::INROOM,ui.in_room_edit->text().toStdString()); }
bool	LocEd::IsSpaceMap()							{ return(fed_map->IsASpaceMap()); }
void	LocEd::LaunchEditCompleteSlot()			{ fed_map->SetLaunch(ui.launch_edit->text().toStdString()); }

void	LocEd::LinkCheckSlot(bool state)			
{ 
	if(!fed_map->SetFlag(Location::LINK,FedMap::CURRENT_LOC,state))
		ui.link_check->setChecked(false); 
}

void	LocEd::LoadActionSlot()			
{ 
	if(!CheckForSave())	// user cancelled operation
		return;

	QString	file_name = QFileDialog::getOpenFileName(this,tr("Open Federation Map File"),data_directory,tr("Map Files (*.loc);;All (*.*)"));
	if(file_name != "")
	{
		QDir	dir(QDir::current()); 
		if(file_name.endsWith(".loc"))
		{
			ClearLocation();
			fed_map->Load(file_name.toStdString());
			if(fed_map->IsASpaceMap())
			{
				fed_map->SetMapType(FedMap::SPACE_MAP);
				ui.launch_edit->clear();
				ui.orbit_edit->clear();
				DisplaySystemName();
			}
			else
				fed_map->SetMapType(FedMap::PLANET_MAP);


			ui.listing_edit->clear();
			ui.map_tab->setCurrentIndex(MAP_TAB);
			ui.info_tab->setCurrentIndex(MAP_INFO);
			EnableEditor();
			SetMapProperties();
			SetLocProperties();
			fed_map->ClearModStatus();
			data_directory = ExtractDataDir(file_name);
		}
		else
			QMessageBox::warning(this,file_name,tr("File name is not a valid map file name."));
	}
}

void	LocEd::LoadSettings()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames","loced");

	settings.beginGroup("MainWindow");
	if(settings.value("fullscreen",false).toBool())
		setWindowState(windowState() ^ Qt::WindowFullScreen);
	else
		resize(settings.value("size", QSize(998,754)).toSize());
	settings.endGroup();

// The commented out code worked in Qt4.8, but doesn't in Qt5.4 et seq for some reason...	
//	QColor color = settings.value("mapbackgroundcolor",Qt::lightGray).value<QColor>();
	QColor color = Qt::lightGray;
	QVariant	var = settings.value("mapbackgroundcolor");
	if(var.isValid())
		color = var.value<QColor>();
	scene->SetBgColor(color);
	data_directory = settings.value("data_dir","./").toString();
}

std::string	LocEd::MakeFullOrbitName()
{
	QString	orbit = ui.system_edit->text();
	orbit += ".";
	orbit += ui.system_edit->text();
	orbit += " Space.";
	orbit += ui.orbit_edit->text();
	return orbit.toStdString();
}

void	LocEd::MapBgColorActionSlot()	
{ 
	QColor	color = QColorDialog::getColor(scene->backgroundBrush().color(),this);
	if(color.isValid())
		scene->SetBgColor(color);
}

void	LocEd::NameCompleteSlot()	{ fed_map->AddName(ui.name_edit->text().toStdString()); }
void	LocEd::NEBtnSlot()			{ ProcessArrowClick(Location::NE);	}
void	LocEd::NECompleteSlot()		{ fed_map->SetExit(Location::NE,ui.ne_text_edit->text().toInt()); }

void	LocEd::NewActionSlot()			
{
	if(!CheckForSave())
		return;

	NewMap	new_map_dialog(this);
	if(new_map_dialog.exec() == QDialog::Accepted)
	{
		ClearLocation();
		ui.loc_no_edit->clear();
		ui.system_edit->clear();
		ui.title_edit->clear();
		ui.launch_edit->clear();
		ui.orbit_edit->clear();
		ui.locs_used_edit->clear();

		fed_map->InitializeNewMap();
		std::string title = new_map_dialog.GetTitle().toStdString();
		FedMap::SanitizeTitle(title);
		fed_map->SetTitle(title);
		system_name = new_map_dialog.GetSystem();
		ui.system_edit->setText(system_name);
		if(new_map_dialog.IsASpaceMap())
			fed_map->SetMapType(FedMap::SPACE_MAP);
		else
			fed_map->SetMapType(FedMap::PLANET_MAP);
		EnableEditor();
		DisplayFileName("");
		SetMapProperties();
		DisableAllLocProperties();
		SetTabsForMap();
	}
}

void	LocEd::NoExitCompleteSlot()	{ fed_map->SetNoExit(ui.no_exit_edit->text().toStdString()); }
void	LocEd::NoMoveEventSlot()		{ fed_map->SetEvent(Location::NOEXIT,ui.no_move_edit->text().toStdString()); }
void	LocEd::NorthBtnSlot()			{ ProcessArrowClick(Location::NORTH); }
void	LocEd::NorthCompleteSlot()		{ fed_map->SetExit(Location::NORTH,ui.north_text_edit->text().toInt()); }

void	LocEd::NotAdjBtnSlot()
{ 
	if(adj_selected)
	{
		fed_map->SetMvtState(FedMap::NORMAL);
		QApplication::setOverrideCursor(Qt::ArrowCursor);
		EnableMvtArrows();
		adj_selected = false;
		return;	
	}

	adj_selected = true;
	QApplication::setOverrideCursor(Qt::CrossCursor);
	fed_map->SetMvtState(FedMap::NON_ADJACENT);
	DisableMvtArrows(ADJ_BTN);
}

void	LocEd::NWBtnSlot()					{ ProcessArrowClick(Location::NW); }
void	LocEd::NWCompleteSlot()				{ fed_map->SetExit(Location::NW,ui.nw_text_edit->text().toInt()); }

void	LocEd::OrbitEditCompleteSlot()	
{ 
	if((ui.system_edit->text() != "") && (ui.orbit_edit->text() != ""))
		fed_map->SetOrbit(MakeFullOrbitName()); 
}

void	LocEd::OutBtnSlot()					{ ProcessDetatchedMove(Location::OUTOF); }
void	LocEd::OutCompleteSlot()			{ fed_map->SetExit(Location::OUTOF,ui.out_text_edit->text().toInt()); }
void	LocEd::PasteActionSlot()			{ fed_map->PasteLoc();	}
void	LocEd::PeaceCheckSlot(bool state)			{ fed_map->SetFlag(Location::PEACE,FedMap::CURRENT_LOC,state); }
void	LocEd::PickupCheckSlot(bool state)			{ fed_map->SetFlag(Location::PICKUP,FedMap::CURRENT_LOC,state); }

void	LocEd::ProcessArrowClick(int direction)
{
	last_arrow_selected = direction;
	int loc_no = fed_map->CurrentLocNo();
	int to_loc = FedMap::Adjacent(last_arrow_selected,loc_no);
	if(to_loc == FedMap::OFF_MAP)
	{
		QMessageBox::information(this,tr("Movement Ignored"),tr("That would cause the player to move off the map!"));
		return;
	}
	if(to_loc == FedMap::WRAP)
	{
		QMessageBox::information(this,tr("Movement Ignored"),tr("That would cause the map to wrap around!"));
		return;
	}
	fed_map->ToggleExit(last_arrow_selected,loc_no);
	fed_map->UpdateScene();
	DisplayExit(direction,to_loc);
}

void	LocEd::ProcessDetatchedMove(int direction)
{
	if((last_arrow_selected == direction) && (fed_map->GetMvtState() == FedMap::NON_ADJACENT)) 
	{
		// User wants to cancel
		fed_map->SetMvtState(FedMap::NORMAL);
		QApplication::setOverrideCursor(Qt::ArrowCursor);
		EnableMvtArrows();
		return;	
	}

	last_arrow_selected = direction;
	QApplication::setOverrideCursor(Qt::CrossCursor);
	fed_map->SetMvtState(FedMap::NON_ADJACENT);
	DisableMvtArrows(direction);
}

void	LocEd::ResetActionSlot()	{ fed_map->ReloadLoc();	}

void	LocEd::SaveActionSlot()		
{ 
	fed_map->SetTitle(ui.title_edit->text().toStdString());
	fed_map->SetLaunch(ui.launch_edit->text().toStdString());
	fed_map->SetOrbit(MakeFullOrbitName());

	fed_map->Save(true);
	fed_map->ClearModStatus();
}

void	LocEd::SaveAsActionSlot()		
{ 
	fed_map->SetTitle(ui.title_edit->text().toStdString());
	fed_map->SetLaunch(ui.launch_edit->text().toStdString());
	fed_map->SetOrbit(MakeFullOrbitName());

	if(!fed_map->CheckMap())
		return;

	QString file_name = QString::fromStdString(GetSaveFileName());
	if(file_name != "")
	{
		fed_map->SaveAs(file_name.toStdString());
		fed_map->ClearModStatus();
		data_directory = ExtractDataDir(file_name);
	}
}

void	LocEd::SaveSettings()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames","loced");

	settings.beginGroup("mainwindow");
	settings.setValue("fullScreen", isFullScreen());
	settings.setValue("size", size());
	settings.endGroup();

	QColor color = scene->GetBgColor();
	settings.setValue("mapbackgroundcolor",color);

	settings.setValue("data_dir",data_directory);
}

void	LocEd::SearchEventSlot()	{ fed_map->SetEvent(Location::SEARCH,ui.search_edit->text().toStdString()); }
void	LocEd::SEBtnSlot()			{ ProcessArrowClick(Location::SE); }
void	LocEd::SECompleteSlot()		{ fed_map->SetExit(Location::SE,ui.se_text_edit->text().toInt()); }

void	LocEd::SetLocProperties()
{
	bool isplanet = !IsSpaceMap();
	ui.space_check->setChecked(!isplanet);
	ui.link_check->setEnabled(!isplanet);
	ui.peace_check->setEnabled(!isplanet);
	ui.fighting_check->setEnabled(!isplanet);
	ui.shipyard_check->setEnabled(isplanet);
	ui.shiprepairs_check->setEnabled(isplanet);
	ui.hospital_check->setEnabled(isplanet);
	ui.exchange_check->setEnabled(isplanet);
	ui.bar_check->setEnabled(isplanet);
	ui.teleport_check->setEnabled(isplanet);
	ui.courier_check->setEnabled(isplanet);
	ui.pickup_check->setEnabled(isplanet);
	ui.weapons_check->setEnabled(isplanet);
	ui.custom_check->setEnabled(true);

	ui.launch_edit->setEnabled(isplanet);
	ui.orbit_edit->setEnabled(isplanet);
	ui.system_edit->setEnabled(true);
}

void	LocEd::SetMapProperties()
{
	bool isplanet = !IsSpaceMap();
	ui.launch_edit->setEnabled(isplanet);
	ui.orbit_edit->setEnabled(isplanet);
	ui.title_edit->setEnabled(isplanet);
}

void	LocEd::SetMenuLoc(bool set_paste)
{
	ui.cut_action->setEnabled(true);
	ui.copy_action->setEnabled(true);
	ui.paste_action->setEnabled(set_paste);
	ui.reset_action->setEnabled(true);
	ui.delete_action->setEnabled(true);
}

void	LocEd::SetMvtValidator()
{
	QIntValidator	*val = new QIntValidator(0,4095,this);

	ui.north_text_edit->setValidator(val);
	ui.south_text_edit->setValidator(val);
	ui.east_text_edit->setValidator(val);
	ui.west_text_edit->setValidator(val);
	ui.ne_text_edit->setValidator(val);
	ui.nw_text_edit->setValidator(val);
	ui.se_text_edit->setValidator(val);
	ui.sw_text_edit->setValidator(val);
	ui.up_text_edit->setValidator(val);
	ui.down_text_edit->setValidator(val);
	ui.in_text_edit->setValidator(val);
	ui.out_text_edit->setValidator(val);
}

void	LocEd::SetTabForLoc()
{
	if(ui.info_tab->currentIndex() != EXIT_INFO)
		ui.info_tab->setCurrentIndex(EXIT_INFO);
}

void	LocEd::SetTabsForMap()
{
	if(ui.map_tab->currentIndex() != MAP_TAB)
		ui.map_tab->setCurrentIndex(MAP_TAB);
	if(ui.info_tab->currentIndex() != MAP_INFO)
		ui.info_tab->setCurrentIndex(MAP_INFO);
}

void	LocEd::SetUpConnections()
{
	SetUpManualEditConnections();
	SetUpMenuConnections();
	SetUpMovementConnections();
	SetUpMiscConnections();
	SetUpPropertyConnections();
}

void	LocEd::SetUpManualEditConnections()
{
	// Exits tab amd main page stuff
	connect(ui.desc_edit,SIGNAL(textChanged()),this,SLOT(DescTextChangedSlot()));
	connect(ui.down_text_edit,SIGNAL(editingFinished()),this,SLOT(CownCompleteSlot()));
	connect(ui.east_text_edit,SIGNAL(editingFinished()),this,SLOT(EastCompleteSlot()));
	connect(ui.in_text_edit,SIGNAL(editingFinished()),this,SLOT(InCompleteSlot()));
	connect(ui.name_edit,SIGNAL(editingFinished()),this,SLOT(NameCompleteSlot()));
	connect(ui.no_exit_edit,SIGNAL(editingFinished()),this,SLOT(NoExitCompleteSlot()));
	connect(ui.north_text_edit,SIGNAL(editingFinished()),this,SLOT(NorthCompleteSlot()));
	connect(ui.ne_text_edit,SIGNAL(editingFinished()),this,SLOT(NECompleteSlot()));
	connect(ui.nw_text_edit,SIGNAL(editingFinished()),this,SLOT(NWCompleteSlot()));
	connect(ui.se_text_edit,SIGNAL(editingFinished()),this,SLOT(SECompleteSlot()));
	connect(ui.south_text_edit,SIGNAL(editingFinished()),this,SLOT(SouththCompleteSlot()));
	connect(ui.sw_text_edit,SIGNAL(editingFinished()),this,SLOT(SWCompleteSlot()));
	connect(ui.up_text_edit,SIGNAL(editingFinished()),this,SLOT(UpCompleteSlot()));
	connect(ui.west_text_edit,SIGNAL(editingFinished()),this,SLOT(WestCompleteSlot()));

	// Events on Events & Properties tab
	connect(ui.enter_edit,SIGNAL(editingFinished()),this,SLOT(EnterEventSlot()));
	connect(ui.in_room_edit,SIGNAL(editingFinished()),this,SLOT(InRoomEventSlot()));
	connect(ui.no_move_edit,SIGNAL(editingFinished()),this,SLOT(NoMoveEventSlot()));
	connect(ui.search_edit,SIGNAL(editingFinished()),this,SLOT(SearchEventSlot()));
}

void	LocEd::SetUpMenuConnections()
{
	connect(ui.about_action,SIGNAL(triggered()),this,SLOT(AboutActionSlot()));
	connect(ui.copy_action,SIGNAL(triggered()),this,SLOT(CopyActionSlot()));
	connect(ui.cut_action,SIGNAL(triggered()),this,SLOT(CutActionSlot()));
	connect(ui.delete_action,SIGNAL(triggered()),this,SLOT(DeleteActionSlot()));
	connect(ui.exit_action,SIGNAL(triggered()),this,SLOT(ExitActionSlot()));
	connect(ui.find_property_action,SIGNAL(triggered()),this,SLOT(FindPropertySlot()));
	connect(ui.find_text_action,SIGNAL(triggered()),this,SLOT(FindTextSlot()));
	connect(ui.font_action,SIGNAL(triggered()),this,SLOT(FontActionSlot()));
	connect(ui.load_action,SIGNAL(triggered()),this,SLOT(LoadActionSlot()));
	connect(ui.map_bg_color_action,SIGNAL(triggered()),this,SLOT(MapBgColorActionSlot()));
	connect(ui.new_action,SIGNAL(triggered()),this,SLOT(NewActionSlot()));
	connect(ui.paste_action,SIGNAL(triggered()),this,SLOT(PasteActionSlot()));
	connect(ui.reset_action,SIGNAL(triggered()),this,SLOT(ResetActionSlot()));
	connect(ui.save_action,SIGNAL(triggered()),this,SLOT(SaveActionSlot()));
	connect(ui.save_as_action,SIGNAL(triggered()),this,SLOT(SaveAsActionSlot()));
}

void	LocEd::SetUpMiscConnections()
{
	connect(ui.launch_edit,SIGNAL(editingFinished()),this,SLOT(LaunchEditCompleteSlot()));
	connect(ui.map_tab,SIGNAL(currentChanged(int)),this,SLOT(TabChangeSlot(int)));
	connect(ui.orbit_edit,SIGNAL(editingFinished()),this,SLOT(OrbitEditCompleteSlot()));
	connect(ui.system_edit,SIGNAL(textChanged(const QString&)),this,SLOT(SystemNameChangedSlot(const QString&)));
	connect(ui.title_edit,SIGNAL(editingFinished()),this,SLOT(TitleEditCompleteSlot()));
}

void	LocEd::SetUpMovementConnections()
{
	connect(ui.down_btn,SIGNAL(clicked()),this,SLOT(DownBtnSlot()));
	connect(ui.east_btn,SIGNAL(clicked()),this,SLOT(EastBtnSlot()));
	connect(ui.in_btn,SIGNAL(clicked()),this,SLOT(InBtnSlot()));
	connect(ui.ne_btn,SIGNAL(clicked()),this,SLOT(NEBtnSlot()));
	connect(ui.north_btn,SIGNAL(clicked()),this,SLOT(NorthBtnSlot()));
	connect(ui.not_adj_btn,SIGNAL(clicked()),this,SLOT(NotAdjBtnSlot()));
	connect(ui.nw_btn,SIGNAL(clicked()),this,SLOT(NWBtnSlot()));
	connect(ui.out_btn,SIGNAL(clicked()),this,SLOT(OutBtnSlot()));
	connect(ui.se_btn,SIGNAL(clicked()),this,SLOT(SEBtnSlot()));
	connect(ui.south_btn,SIGNAL(clicked()),this,SLOT(SouthBtnSlot()));
	connect(ui.sw_btn,SIGNAL(clicked()),this,SLOT(SWBtnSlot()));
	connect(ui.up_btn,SIGNAL(clicked()),this,SLOT(UpBtnSlot()));
	connect(ui.west_btn,SIGNAL(clicked()),this,SLOT(WestBtnSlot()));
}

void	LocEd::SetUpPropertyConnections()
{
	connect(ui.bar_check,SIGNAL(clicked(bool)),this,SLOT(BarCheckSlot(bool)));
	connect(ui.courier_check,SIGNAL(clicked(bool)),this,SLOT(CourierCheckSlot(bool)));
	connect(ui.custom_check,SIGNAL(clicked(bool)),this,SLOT(CustomCheckSlot(bool)));
	connect(ui.exchange_check,SIGNAL(clicked(bool)),this,SLOT(ExchangeCheckSlot(bool)));
	connect(ui.fighting_check,SIGNAL(clicked(bool)),this,SLOT(FightingCheckSlot(bool)));
	connect(ui.hospital_check,SIGNAL(clicked(bool)),this,SLOT(HospitalCheckSlot(bool)));
	connect(ui.link_check,SIGNAL(clicked(bool)),this,SLOT(LinkCheckSlot(bool)));
	connect(ui.peace_check,SIGNAL(clicked(bool)),this,SLOT(PeaceCheckSlot(bool)));
	connect(ui.pickup_check,SIGNAL(clicked(bool)),this,SLOT(PickupCheckSlot(bool)));
	connect(ui.shiprepairs_check,SIGNAL(clicked(bool)),this,SLOT(ShipRepairsCheckSlot(bool)));
	connect(ui.shipyard_check,SIGNAL(clicked(bool)),this,SLOT(ShipyardCheckSlot(bool)));
	connect(ui.space_check,SIGNAL(clicked(bool)),this,SLOT(SpaceCheckSlot(bool)));
	connect(ui.teleport_check,SIGNAL(clicked(bool)),this,SLOT(TeleportCheckSlot(bool)));
	connect(ui.weapons_check,SIGNAL(clicked(bool)),this,SLOT(WeaponsCheckSlot(bool)));
}

void	LocEd::ShipRepairsCheckSlot(bool state)	{ fed_map->SetFlag(Location::REPAIR,FedMap::CURRENT_LOC,state); }
void	LocEd::ShipyardCheckSlot(bool state)		{ fed_map->SetFlag(Location::SHIPYARD,FedMap::CURRENT_LOC,state); }

void	LocEd::ShowErrorMessage(const std::string& mssg)
{
	QMessageBox::critical(this,tr("Error"),QString::fromStdString(mssg));
}

void	LocEd::SouthBtnSlot()					{ ProcessArrowClick(Location::SOUTH); }
void	LocEd::SouthCompleteSlot()				{ fed_map->SetExit(Location::SOUTH,ui.south_text_edit->text().toInt()); }
void	LocEd::SpaceCheckSlot(bool state)	{ fed_map->SetFlag(Location::SPACE,FedMap::CURRENT_LOC,state); }

void	LocEd::SWBtnSlot()						{ ProcessArrowClick(Location::SW); }
void	LocEd::SWCompleteSlot()					{ fed_map->SetExit(Location::SW,ui.sw_text_edit->text().toInt()); }

void	LocEd::SystemNameChangedSlot(const QString& new_text)
{
	if((new_text == system_name) && (new_text == QString::fromStdString(fed_map->GetTitle())))
		return;

	system_name = new_text;
	if(IsSpaceMap())
	{
		if(new_text == "")
			ui.title_edit->clear();
		else
			ui.title_edit->setText(ui.system_edit->text() + QString(" Space"));
		fed_map->SetTitle(ui.title_edit->text().toStdString());
	}

	fed_map->SetModified();
}

void	LocEd::TabChangeSlot(int new_tab)
{ 
	ui.listing_edit->clear();
	if(new_tab == LIST_TAB)
 		fed_map->WriteListing();
}

void	LocEd::TeleportCheckSlot(bool state)	{ fed_map->SetFlag(Location::TELEPORT,FedMap::CURRENT_LOC,state); }
void	LocEd::TitleEditCompleteSlot()			{ fed_map->SetTitle(ui.title_edit->text().toStdString()); }
void	LocEd::UpBtnSlot()							{ ProcessDetatchedMove(Location::UP); }
void	LocEd::UpCompleteSlot()						{ fed_map->SetExit(Location::UP,ui.up_text_edit->text().toInt()); }

void	LocEd::WeaponsCheckSlot(bool state)		
{ 
	if(!fed_map->SetFlag(Location::WEAPONS,FedMap::CURRENT_LOC,state))
		ui.weapons_check->setChecked(false); 
}

void	LocEd::WestBtnSlot()							{ ProcessArrowClick(Location::WEST);  }
void	LocEd::WestCompleteSlot()					{ fed_map->SetExit(Location::WEST,ui.west_text_edit->text().toInt()); }


/* ---------------------- Work in Progress ---------------------- */

