/*-----------------------------------------------------------------------
             LocEd - Windows Federation 2 Map Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "scene.h"

#include <QtGlobal>
#include <QGraphicsSceneMouseEvent>
#include <QList>

#include "fedmap.h"
#include "tiles.h"

MapScene::MapScene(FedMap *f_map,QObject *parent) : QGraphicsScene(parent)
{
	height = FedMap::HEIGHT * 2 - 1;
	width = FedMap::WIDTH * 2 - 1;

	setSceneRect(0,0,width * Tiles::WIDTH,height * Tiles::HEIGHT);
	background_color = QColor::fromRgb(180,180,180);
	setBackgroundBrush(background_color);

	fed_map = f_map;
	tiles = new Tiles;

	InitialiseMap();
}

MapScene::~MapScene()	
{	
	delete tiles;
}

void	MapScene::AddTile(int abs_row,int abs_column,int tile_num)
{
	QGraphicsItem *item = addPixmap(*tiles->Tile(tile_num));
	item->setPos(abs_row*Tiles::HEIGHT,abs_column*Tiles::WIDTH);
}

void	MapScene::ChangeTile(int abs_row,int abs_column,int tile_num)
{
	QGraphicsItem *item = itemAt(QPointF(qreal(abs_row * Tiles::HEIGHT),qreal(abs_column * Tiles::WIDTH)),QTransform());	
	if(item != nullptr)
		removeItem(item);
	delete item;

	item = addPixmap(*tiles->Tile(tile_num));
	item->setPos(abs_row*Tiles::HEIGHT,abs_column*Tiles::WIDTH);
}

void	MapScene::InitialiseMap()
{
	clear();
	setBackgroundBrush(QColor(background_color));
	
	QPixmap *pix_map = tiles->Tile(Tiles::UNUSED);
	QGraphicsPixmapItem *item;
	for(int ht = 0;ht < height;++ht)
	{
		for(int wd = 0;wd < width;++wd)
		{
			item = addPixmap(*pix_map);
			item->setPos(ht*32,wd*32);
		}
	}
}

void	MapScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	QPoint	pos = mouseEvent->scenePos().toPoint();
	int abs_row = pos.y()/Tiles::WIDTH;
	int abs_column = pos.x()/Tiles::HEIGHT;

	fed_map->MouseAt(abs_row,abs_column);
}

void MapScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	QPointF	pos = mouseEvent->scenePos();
	int abs_row = pos.y()/Tiles::WIDTH;
	int abs_column = pos.x()/Tiles::HEIGHT;

	fed_map->ClickAt(abs_row,abs_column);
}

// There appear to be two copies of the first arrow laid down in cases
// where there are multiple arrows. Need to look into why this should be, eventually...
void	MapScene::RemoveTile(int abs_row,int abs_column,int tile_num)
{
	QList<QGraphicsItem *> item_list = items(abs_row*Tiles::HEIGHT,abs_column*Tiles::WIDTH,
										Tiles::WIDTH,Tiles::HEIGHT,Qt::IntersectsItemShape,Qt::AscendingOrder);
	int	size = item_list.size();
	if(size > 0)
	{
		qint64	tile_hash =  tiles->GetCacheKey(tile_num);
		for(int count = 0;count < size;++count)
		{
			if(dynamic_cast<QGraphicsPixmapItem *>(item_list[count])->pixmap().cacheKey() == tile_hash)
			{
				QGraphicsItem * item = item_list[count];
				removeItem(item_list[count]);
				delete item;
			}
		}
	}
}

void	MapScene::SetBgColor(const QColor& color)
{
	background_color = color;
	setBackgroundBrush(background_color);
}

QColor MapScene::GetBgColor()
{
	return(backgroundBrush().color());
}